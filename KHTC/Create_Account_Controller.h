//
//  Create_Account_Controller.h
//  KHTC
//
//  Created by Hans Yelek on 8/26/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    Create_Account_Controller
    
        (this controller is on storyboard)
 
        * this controller is displayed to the user while creating a new account
        * after the user finished data entry, an http request is made to the atcj.org server
            and the new account data is saved into the database
 
 *******/

#import <UIKit/UIKit.h>

@class Inset_TextField;

@interface Create_Account_Controller : UITableViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet Inset_TextField *emailField;
@property (weak, nonatomic) IBOutlet Inset_TextField *passwordField;
@property (weak, nonatomic) IBOutlet Inset_TextField *confirmPassField;
@property (weak, nonatomic) IBOutlet Inset_TextField *firstNameField;
@property (weak, nonatomic) IBOutlet Inset_TextField *lastNameField;
@property (weak, nonatomic) IBOutlet Inset_TextField *phoneField;
@property (weak, nonatomic) IBOutlet Inset_TextField *cityField;
@property (weak, nonatomic) IBOutlet Inset_TextField *stateField;

- (IBAction)createNewAccount:(id)sender;


@end
