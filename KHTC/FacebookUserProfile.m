//
//  FacebookUserProfile.m
//  KHTC
//
//  Created by Hans Yelek on 6/16/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "FacebookUserProfile.h"


@implementation FacebookUserProfile

@dynamic carMake;
@dynamic carModel;
@dynamic city;
@dynamic email;
@dynamic firstName;
@dynamic isSignedIn;
@dynamic lastName;
@dynamic password;
@dynamic phone;
@dynamic state;
@dynamic facebookID;
@dynamic profileImageData;

@end
