//
//  GooglePlacesAutoCompleteCommand.h
//  KHTC
//
//  Created by Hans Yelek on 7/3/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    GooglePlacesAutoCompleteCommand
 
        - sends a request to the Google Places AutoComplete API
        - this API predicts what place a user is typing into a text field and
            returns a JSON object with possible matches
    
        - with these matches, a UITableView, say, can be filled with the predicted
            places
 
 *******/

#import "BaseCommand.h"

@interface GooglePlacesAutoCompleteCommand : BaseCommand

@property (nonatomic, strong) NSString *place;

@end
