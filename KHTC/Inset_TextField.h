//
//  Inset_TextField.h
//  KHTC
//
//  Created by Hans Yelek on 8/26/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*
    Inset_TextField
        
        * custom text field with text right-indented from left boundary
 
 */

#import <UIKit/UIKit.h>

@interface Inset_TextField : UITextField

@end
