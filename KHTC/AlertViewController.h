//
//  AlertViewController.h
//  KHTC
//
//  Created by Hans Yelek on 8/23/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    AlertViewController
        - (this controller has a view on storyboard)
    
    * This controller displays data for a single alert. It is pushed on top of a 
    * Nearby_Alerts_Controller, which displays a list of possibly several alerts.
 
 *******/

#import <UIKit/UIKit.h>

@class AmberAlert;
@class MKMapView;

@interface AlertViewController : UITableViewController
{
}

@property (nonatomic, strong) NSDictionary *amberAlert;         // contains all alert information
@property (nonatomic, strong) UIImage *animalImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *datePostedLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView_;

@end
