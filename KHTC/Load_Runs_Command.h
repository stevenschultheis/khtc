//
//  Load_Runs_Command.h
//  KHTC
//
//  Created by Hans Yelek on 9/4/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
    Load_Runs_Command
 
        - loads runs from database corresponding to the user with id khtcID
 
 *******/

#import "BaseCommand.h"

@interface Load_Runs_Command : BaseCommand

@property (nonatomic) NSNumber * khtcID;       // user's unique id in atcj.org database

@end
