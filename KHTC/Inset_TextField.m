//
//  Inset_TextField.m
//  KHTC
//
//  Created by Hans Yelek on 8/26/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "Inset_TextField.h"

@implementation Inset_TextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - UITextField Overrides

// text inset for placeholder
- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10, 10);
}

// text inset for text
- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10, 10);
}

@end