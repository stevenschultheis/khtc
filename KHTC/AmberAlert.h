//
//  AmberAlert.h
//  KHTC
//
//  Created by Hans Yelek on 6/26/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    AmberAlert
        
        * contains information relevant to a single missing animal alert
        * an object of this class is created when the user is creating a 
            new "amber alert," or when a list of existing amber alerts is
            requested from the atcj.org database in order to display to the user
 
 *******/


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface AmberAlert : NSObject

@property (nonatomic, strong) NSString *animalName;
//@property (nonatomic, strong) NSString *animalType;
@property (nonatomic, strong) NSString *animalBreed;
@property (nonatomic, strong) NSString *contactPhone;

@property (nonatomic, strong) UIImage *animalImage;
@property (nonatomic, assign) CLLocationCoordinate2D lastKnownLocationCoordinate;
@property (nonatomic, strong) NSString *lastKnownLocationName;

+ (AmberAlert *)sharedAmberAlert;

@end
