//
//  Runs_View_Controller.m
//  KHTC
//
//  Created by Hans Yelek on 9/4/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "Runs_View_Controller.h"
#import "Push_Notification_Command_TEST.h"
#import "Constants.h"

#define kRunTitleCellHeight     88
#define kRunDetailCellHeight    66
#define kRunLegCellHeight       66
#define kPetDetailCellHeight    66

@interface Runs_View_Controller ()

@end

@implementation Runs_View_Controller

@synthesize runs = _runs;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([[_runs objectForKey:@"results"]count] == 1) {
        assignedOneRun = YES;
        NSLog(@"DRIVERS\n%@",[[[_runs objectForKey:@"results"] valueForKey:@"run"] valueForKey:@"drivers"]);
        runCoordinator = [[[_runs objectForKey:@"results"] valueForKey:@"run" ]valueForKey:@"runCoordinator"];
        drivers = [[[_runs objectForKey:@"results" ]valueForKey:@"run"] valueForKey:@"drivers"];
        pets = [[[_runs objectForKey:@"results"] valueForKey:@"run"] valueForKey:@"pets"];
    }
    else
        assignedOneRun = NO;
    
    NSArray *values = [_runs allValues];
    NSLog(@"values: %@", values);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource Protocol Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (assignedOneRun)
        //return 3 + [drivers count];
        return 3 + [[drivers objectAtIndex:0] count];   // run log, run coordinator, pets, + driver legs
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSArray *a = [[[_runs objectForKey:@"results"] valueForKey:@"run"] valueForKey:@"drivers"];
    if (assignedOneRun) {
        if (section == 0) { /* Run Log Section */
            return 1;
        } else if (section == 1) {  /* Run Coordinator Section */
            return 2;
        } else if (section <= [[drivers objectAtIndex:0] count] + 1) {    /* Run Legs Sections */
            return 5;
        } else {    /* Pet Section */
            return 1 + [[pets objectAtIndex:0] count]; /* title cell plus pet cells */
        }
    }
    /* User has been assigned multiple runs */
    else {
        return 0;   /* change this */
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *runTitleCellID = @"runTitleCell";
    static NSString *runDetailCellID = @"runDetailCell";
    static NSString *runLegCellID = @"runLegCell";
    static NSString *petNameCellID = @"petNameCell";
    
    UITableViewCell *cell;
    NSInteger section = indexPath.section;

    if (assignedOneRun) {
        if (section == 0) { /* Run Log Section */
            if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:runTitleCellID];
            }
        } else if (section == 1) {  /* Run Coordinator Section */
            switch (indexPath.row) {
                case 0: /* Title Cell */
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:runTitleCellID];
                    UILabel *title = (UILabel *)[cell viewWithTag:1];
                    [title setText:@"Run Coordinator"];
                    break;
                }
                case 1: /* Run Coordinator Name Cell */
                    cell = [tableView dequeueReusableCellWithIdentifier:runDetailCellID];
                    UILabel *category = (UILabel *)[cell viewWithTag:1];
                    [category setText:@"Name"];
                    UILabel *runCoordName = (UILabel *)[cell viewWithTag:2];
                    [runCoordName setText:[[runCoordinator objectAtIndex:0] valueForKey:@"name"]];
                    break;
            }
            
        } else if (section <= [[drivers objectAtIndex:0] count] + 1) {    /* Run Legs Sections */
            NSArray *driver = [[drivers objectAtIndex:0] objectAtIndex:section-2];
            switch (indexPath.row) {
                case 0: /* Run Leg Title Cell */
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:runLegCellID];
                    UILabel *title = (UILabel *)[cell viewWithTag:1];
                    [title setText:[NSString stringWithFormat:@"Leg %d", section - 1]];
                    break;
                }
                case 1: /* Driver Cell */
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:runDetailCellID];
                    UILabel *category = (UILabel *)[cell viewWithTag:1];
                    [category setText:@"driver"];
                    UILabel *driverName = (UILabel *)[cell viewWithTag:2];
                    [driverName setText:[NSString stringWithFormat:@"%@", [driver valueForKey:@"name" ]]];
                    break;
                }
                case 2: /* Start Location Cell */
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:runDetailCellID];
                    UILabel *category = (UILabel *)[cell viewWithTag:1];
                    [category setText:@"start"];
                    UILabel *start = (UILabel *)[cell viewWithTag:2];
                    [start setText:[driver valueForKey:@"startLocation"]];
                    break;
                }
                case 3: /* Finish Location Cell */
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:runDetailCellID];
                    UILabel *category = (UILabel *)[cell viewWithTag:1];
                    [category setText:@"finish"];
                    UILabel *finish = (UILabel *)[cell viewWithTag:2];
                    [finish setText:[driver valueForKey:@"finishLocation"]];
                    break;
                }
                case 4: /* Status Cell */
                    cell = [tableView dequeueReusableCellWithIdentifier:runDetailCellID];
                    UILabel *category = (UILabel *)[cell viewWithTag:1];
                    [category setText:@"status"];
                    UILabel *status = (UILabel *)[cell viewWithTag:2];
                    [status setText:@"Not Started"];
                    break;
            }
        } else {    /* Pet Section */
            if (indexPath.row == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:runTitleCellID];
                UILabel *title = (UILabel *)[cell viewWithTag:1];
                [title setText:@"Animals"];
            } else {
                cell = [tableView dequeueReusableCellWithIdentifier:petNameCellID];
                UILabel *petName = (UILabel *)[cell viewWithTag:1];
                [petName setText:[[[pets objectAtIndex:0] objectAtIndex:indexPath.row-1 ] valueForKey:@"name"]];
            }
        }
    return cell;
    }
    // user assigned multiple runs
    else {
        /* CHANGE THIS */
        return [tableView dequeueReusableCellWithIdentifier:runTitleCellID];
    }
}

#pragma mark - UITableViewDelegate Protocol Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    if (assignedOneRun) {
        if (section == 0) { /* Run Log Section */
            return kRunTitleCellHeight;
        } else if (section == 1) {  /* Run Coordinator Section */
            switch (indexPath.row) {
                case 0:
                    return kRunTitleCellHeight;
                    break;
                case 1:
                    return kRunDetailCellHeight;
                    break;
                default:
                    return 0;
            }
        } else if (section <= [[drivers objectAtIndex:0] count] + 1) {/* Run Legs Sections */
            if (indexPath.row == 0)
                return kRunLegCellHeight;
            else return kRunDetailCellHeight;
        } else {    /* Pet Section */
            if (indexPath.row == 0)
                return kRunTitleCellHeight;
            else return kPetDetailCellHeight;
        }
    } else {
        return 0; /* CHANGE THIS */
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)pushNotificationTest:(id)sender
{
    Push_Notification_Command_TEST *command = [[Push_Notification_Command_TEST alloc] init];
    [command setVolunteerID:@"1032"];
    [command setRunID:@"30"];
    
    NSString *body = @"{\"aps\": {\"alert\":\"Your First Push Notification\",\"badge\":1}}";
    [command setNotificationBody:body];
    [command listenForMyCompletion:self
                          selector:@selector(pushNotificationCmdComplete:)
                  notificationName:kDidCompletePushNotificationOp];
    [command enqueueOperation];
}

#pragma mark - Notification Methods

// Notification: kDidCompletePushNotification
// Sent by Push_Notification_Command_TEST object
- (void)pushNotificationCmdComplete:(NSNotification *)notification
{
    /* 
     
     should confirm with user that remote notification was sent if successful
     - refer to other notification methods in other classes to see how to 
        check command status, etc.
     
     */
    
}

@end