//
//  Push_Notification_Command_TEST.m
//  KHTC
//
//  Created by Hans Yelek on 9/29/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "Push_Notification_Command_TEST.h"
#import "Constants.h"

@implementation Push_Notification_Command_TEST

@synthesize volunteerID = _volunteerID;
@synthesize runID = _runID;
@synthesize notificationBody = _notificationBody;

- (void)main
{
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:
                             _volunteerID, @"volunteerID",
                             _runID, @"runID",
                             _notificationBody, @"notificationBody", nil];
    NSError *error = nil;
    NSData *dataPayload = [NSJSONSerialization dataWithJSONObject:payload
                                                          options:NSJSONWritingPrettyPrinted
                                                            error:&error];
    if (error != nil) {
        NSLog(@"error: %@", [error localizedDescription]);
        abort();
    }
    
    NSURL *url = [NSURL URLWithString:@"http://atcj.org/KHTC/PushNotifications/push_notification.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:dataPayload];
    [request setHTTPMethod:@"POST"];
    
    NSHTTPURLResponse *response = nil;
    NSData *data = [self sendSynchronousRequest:request
                                     response_p:&response
                                          error:&error];
    NSLog(@"Script Data: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    
    // Check for OS error
    if (error != nil) {
        NSLog(@"error: %@", [error localizedDescription]);
        abort();
    }
    
    self.httpStatusCode = [response statusCode];
    NSDictionary *responsePayload = nil;
    
    // Check for HTTP Error
    /* OK */
    if (self.httpStatusCode == 200) {
        self.status = kSuccess;
        
    }
    /* HTTP Error */
    else {
        self.status = kFailure;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompletePushNotificationOp object:self
userInfo:responsePayload];
}

@end
