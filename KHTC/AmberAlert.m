//
//  AmberAlert.m
//  KHTC
//
//  Created by Hans Yelek on 6/26/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "AmberAlert.h"

@implementation AmberAlert

static AmberAlert *_sharedAmberAlert = nil;

@synthesize animalName = _animalName;
//@synthesize animalType = _animalType;
@synthesize animalBreed = _animalBreed;
@synthesize animalImage = _animalImage;

@synthesize contactPhone = _contactPhone;
@synthesize lastKnownLocationCoordinate = _lastKnownLocationCoordinate;
@synthesize lastKnownLocationName = _lastKnownLocationName;

#pragma mark - Class Methods

+ (AmberAlert *)sharedAmberAlert
{
    @synchronized([AmberAlert class]) {
        if (! _sharedAmberAlert) 
            _sharedAmberAlert = [[self alloc] init];
        
        return _sharedAmberAlert;
    }
    return nil;
}

@end
