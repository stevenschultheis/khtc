//
//  Push_Notification_Command_TEST.h
//  KHTC
//
//  Created by Hans Yelek on 9/29/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    Push_Notification_Command_TEST
        
        - sends out a remote notification initiated by the user
        - for example, if a user/driver sends out a notice of arrival,
          the remote notification is sent through an instance of this class.
 
 *******/

#import "BaseCommand.h"

@interface Push_Notification_Command_TEST : BaseCommand

@property (nonatomic, strong) NSString *volunteerID;
@property (nonatomic, strong) NSString *runID;
@property (nonatomic, strong) NSString *notificationBody;

@end
