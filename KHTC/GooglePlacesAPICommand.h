//
//  GooglePlacesAPICommand.h
//  KHTC
//
//  Created by Hans Yelek on 7/4/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
    
    GooglePlacesAPICommand
 
        - this command sends a request to the Google Places API with a string
            that presumably describes a place (see property below)
 
        - the API returns information relevant to that place, including lat/lng
            coordinates
 
 *******/

#import "BaseCommand.h"

@interface GooglePlacesAPICommand : BaseCommand

@property (nonatomic, strong) NSString *place;

@end
