//
//  Find_Member_View_Controller.h
//  KHTC
//
//  Created by Hans Yelek on 8/23/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
    
    Find_Member_View_Controller
 
        (controller on storyboard)
 
        * this controller allows user to search for a preexisting account in the atcj.org database
 
            - if user is found, need to save user profile locally
                see searchedForMember: method in .m file
 
 *******/

#import <UIKit/UIKit.h>

@interface Find_Member_View_Controller : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>
{
    BOOL enteringPassword;  // YES when the textfields are used to input a password
    NSString * khtcID;             // identifies the user's unique id in the atcj.org database
}

@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomImageView;
@property (weak, nonatomic) IBOutlet UILabel *userPrompt;

@end
