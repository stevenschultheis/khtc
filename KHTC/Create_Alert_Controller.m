//
//  Create_Alert_Controller.m
//  KHTC
//
//  Created by Hans Yelek on 8/21/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*
 TO DO LIST:
    - form validation!
 */

#import "Create_Alert_Controller.h"
#import "LastLocationController.h"
#import "CreateAmberAlertCommand.h"
#import "CameraOrRollAlert.h"
#import "AmberAlert.h"
#import "Constants.h"

@interface Create_Alert_Controller ()

@end

@implementation Create_Alert_Controller 

@synthesize amberAlert = _amberAlert;
@synthesize animalImage = _animalImage;
@synthesize nameTextField = _nameTextField;
@synthesize breedTextField = _breedTextField;
@synthesize phoneTextField = _phoneTextField;
@synthesize cameraAlertView = _cameraAlertView;
@synthesize locationLabel = _locationLabel;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    _amberAlert = [AmberAlert sharedAmberAlert];
 
    [_nameTextField setDelegate:self];
    [_breedTextField setDelegate:self];
    [_phoneTextField setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)getLastLocation
{
    LastLocationController *lastLocationController = [[LastLocationController alloc] initWithNibName:@"LastLocationController" bundle:nil];
    [lastLocationController setDelegate:self];
    [self presentViewController:lastLocationController animated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate Protocol Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Last Known Location cell selected
    if (indexPath.section == 2 && indexPath.row == 0) {
        [self getLastLocation];
    }
}

#pragma mark - UIImagePickerControllerDelegate Protocol Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *img = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [_amberAlert setAnimalImage:img];
    [_animalImage setImage:img];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - LastLocationControllerDelegate Protocol Methods

- (void)lastLocationControllerDidReturnWithPlace:(NSString *)place andCoordinates:(CLLocationCoordinate2D)coordinates
{
    [_locationLabel setText:place];
}

#pragma mark - Notification Methods

// Notification: kDidCompleteCreateAmberAlertOpNotification
// sent by CreateAmberAlertCommand object
- (void)createAlertCommandCompleted:(NSNotification *)notification
{
    CreateAmberAlertCommand *amberAlertCommand = [notification object];
    if (amberAlertCommand.status == kSuccess) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:@"Amber Alert"
                                        message:@"Amber Alert Created!"
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:@"Amber Alert"
                                        message:@"Amber Alert Creation Failed"
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        });
    }
}

#pragma mark - IBAction Methods

- (IBAction)newPhoto:(id)sender
{
    [self dismissCameraOrRollView:nil];
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePickerController setDelegate:self];
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (IBAction)photoFromCameraRoll:(id)sender
{
    [self dismissCameraOrRollView:nil];
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [imagePickerController setDelegate:self];
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (IBAction)dismissCameraOrRollView:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        [_cameraAlertView setFrame:CGRectMake(0, 800, 320, 460)];
    }];
    
}

- (IBAction)createNewAlert:(id)sender
{
    // form validation first!
    // check that all fields are complete and that phone number
    // is valid
    //
    
    
    [_amberAlert setAnimalName:_nameTextField.text];
    [_amberAlert setAnimalBreed:_breedTextField.text];
    [_amberAlert setContactPhone:_phoneTextField.text];
    
    // (picture and location are set in the UIImagePickerController and
    // LastLocationController classes, respectively
    
    // After alert is created, send it to the server for saving
    CreateAmberAlertCommand *amberAlertCommand = [[CreateAmberAlertCommand alloc] init];
    amberAlertCommand.amberAlert = _amberAlert;
    [amberAlertCommand listenForMyCompletion:self
                                    selector:@selector(createAlertCommandCompleted:)
                            notificationName:kDidCompleteCreateAmberAlertOpNotification];
    [amberAlertCommand enqueueOperation];
}



- (IBAction)showCameraOrRollAlertView:(id)sender
{
    // Show alert view to allow user to create new photo or choose existing one from camera roll
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        if (! _cameraAlertView) {
            _cameraAlertView = [[[NSBundle mainBundle] loadNibNamed:@"CameraOrRollAlert" owner:self options:nil] objectAtIndex:0];
        }
        [_cameraAlertView setFrame:CGRectMake(0, 460, 320, 460)];
        [_cameraAlertView setHidden:NO];
        [self.view addSubview:_cameraAlertView];
        
        [UIView animateWithDuration:0.3 animations:^{
            [_cameraAlertView setFrame:CGRectMake(0, 0, 320, 460)];
        }];
    }
    // If camera is not available, just send user to camera roll
    else {
        
    }
}

@end