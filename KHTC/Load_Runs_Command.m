//
//  Load_Runs_Command.m
//  KHTC
//
//  Created by Hans Yelek on 9/4/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "Load_Runs_Command.h"
#import "Constants.h"

@implementation Load_Runs_Command

@synthesize khtcID = _khtcID;

- (void)main
{
    // Create JSON payload
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:
                             _khtcID, @"id", nil];
    
    NSError *error = nil;
    NSData *dataPayload = [NSJSONSerialization dataWithJSONObject:payload
                                                          options:NSJSONWritingPrettyPrinted
                                                            error:&error];
    
    if (error != nil) {
        NSLog(@"Error serializing JSON object: %@", [error localizedDescription]);
        // error handling here...
        abort();
    }
    
    NSURL *url = [NSURL URLWithString:@"http://atcj.org/KHTC/Runs/get_user_runs.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:dataPayload];
    [request setHTTPMethod:@"POST"];
    
    NSHTTPURLResponse *response = nil;
    NSData *data = [self sendSynchronousRequest:request
                                     response_p:&response
                                          error:&error];
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"data string: %@    END DATA STRING", dataString);
    // Check for OS error
    if (error != nil) {
        NSLog(@"OS Error: %@", [error localizedDescription]);
        // error handling here...
        abort();
    }
    
    self.httpStatusCode = [response statusCode];
    NSDictionary *responsePayload = nil;
    
    // Check for HTTP error
    /* OK */
    if (self.httpStatusCode == 200) {
        responsePayload = [NSJSONSerialization JSONObjectWithData:data
                                                          options:NSJSONReadingMutableContainers
                                                            error:&error];
        NSLog(@"json response:\n%@", responsePayload);
        
        /* check for error on server side */
        // ...
        self.status = kSuccess;
    }
    /* HTTP Error */
    else {
        NSLog(@"HTTP Error: %ld", (long)[response statusCode]);
        self.status = kFailure;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompleteFindRunsOpNotification
                                                    object:self
                                                  userInfo:responsePayload];
}

@end