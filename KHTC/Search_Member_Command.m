//
//  Search_Member_Command.m
//  KHTC
//
//  Created by Hans Yelek on 8/23/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "Search_Member_Command.h"
#import "Constants.h"

@implementation Search_Member_Command

@synthesize email = _email;
@synthesize firstName = _firstName;

- (void)main
{
    // create JSON payload
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          _email, @"email",
                          _firstName, @"firstName", nil];
    NSError *error = nil;
    NSData *payload = [NSJSONSerialization dataWithJSONObject:dict
                                                      options:NSJSONWritingPrettyPrinted
                                                        error:&error];
    if (error != nil) {
        NSLog(@"Error creating JSON payload: %@", [error localizedDescription]);
        // error handling here...
        return;
    }
    
    // create and send URL request
    NSURL *url = [NSURL URLWithString:@"http://atcj.org/KHTC/Account/search_for_member.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:payload];
    
    NSHTTPURLResponse *response = nil;
    
    NSData *data = [self sendSynchronousRequest:request
                                     response_p:&response
                                          error:&error];
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"data: %@", string);
    
    // Check for OS error
    //
    if (error != nil) {
        NSLog(@"OS Error Making URL Request: %@", [error localizedDescription]);
        NSLog(@"HTTP Status Code: %ld", (long)[response statusCode]);
        NSLog(@"HTTP Body: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        // error handling here...
        return;
    }
    
    self.httpStatusCode = [response statusCode];
    NSDictionary *responsePayload = nil;
    // Check for HTTP error
    // OK
    if (self.httpStatusCode == 200) {
        self.status = kSuccess;
        
        responsePayload = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers
                                                            error:&error];
        if (error != nil) {
            NSLog(@"error converting return payload to json: %@, %@", error, [error userInfo]);
            NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"data: %@", string);
            abort();
        }
    }
    // HTTP error
    else {
        self.status = kFailure;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompleteSearchMemberOpNotification object:self userInfo:responsePayload];
}

@end
