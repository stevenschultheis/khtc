//
//  Save_Password_Command.h
//  KHTC
//
//  Created by Hans Yelek on 8/28/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    Save_Password_Command
 
        - saves the user's account password into password
        
    IMPORTANT NOTE:
        - currently, the passwords are not encrypted. This is NOT OK. Before the app goes public,
            the passwords must be encrypted with RSA or by some other encryption method.
 
 *******/

#import "BaseCommand.h"

@interface Save_Password_Command : BaseCommand

@property (nonatomic, strong) NSString *khtcID;
@property (nonatomic, strong) NSString *password;

@end
