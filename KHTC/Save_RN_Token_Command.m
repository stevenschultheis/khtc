//
//  Save_RN_Token_Command.m
//  KHTC
//
//  Created by Hans Yelek on 9/28/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "Save_RN_Token_Command.h"
#import "Constants.h"

@implementation Save_RN_Token_Command

@synthesize khtcID = _khtcID;
@synthesize token = _token;

- (void)main
{
    // Create JSON object
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:
                             _khtcID, @"volunteerID",
                             _token, @"token", nil];
    NSError *error = nil;
    NSData *dataPayload = [NSJSONSerialization dataWithJSONObject:payload
                                                          options:NSJSONWritingPrettyPrinted
                                                            error:&error];
    if (error != nil) {
        NSLog(@"Error serializing JSON object: %@", [error localizedDescription]);
        // error handling here
        abort();
    }
    
    NSURL *url = [NSURL URLWithString:@"http://atcj.org/KHTC/Account/save_rn_token.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:dataPayload];
    [request setHTTPMethod:@"POST"];
    
    NSHTTPURLResponse *response = nil;
    NSData *data = [self sendSynchronousRequest:request
                                     response_p:&response
                                          error:&error];
    // Check for OS error
    if (error != nil) {
        NSLog(@"OS Error: %@", [error localizedDescription]);
        // error handling here ...
        abort();
    }
    NSLog(@"data: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    
    self.httpStatusCode = [response statusCode];
    NSDictionary *responsePayload = nil;
    
    //NSLog(@"error: %@", [error localizedDescription]);
    // Check for HTTP error
    /* OK */
    if (self.httpStatusCode == 200) {
        
        self.status = kSuccess;
        responsePayload = [NSJSONSerialization JSONObjectWithData:data
                                                          options:NSJSONReadingMutableContainers
                                                            error:&error];
        NSLog(@"Payload:\n%@", responsePayload);

    }
    /* HTTP Error */
    else {
        NSLog(@"HTTP Error: %ld", (long)[response statusCode]);
        NSLog(@"Error:\n%@", responsePayload);
        self.status = kFailure;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompleteRegisterRNTokenOpNotification
                                                        object:self
                                                      userInfo:responsePayload];
}

@end
