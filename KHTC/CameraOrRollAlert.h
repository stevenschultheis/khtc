//
//  CameraOrRollAlert.h
//  KHTC
//
//  Created by Hans Yelek on 7/1/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    CameraOrRollAlert
 
        - custom alert view reqesting photo type desired by user:
            1) existing photo from camera roll,
            2) new photo with camera
 
 *******/

#import <UIKit/UIKit.h>

@interface CameraOrRollAlert : UIView

@end
