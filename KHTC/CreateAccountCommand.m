//
//  CreateAccountCommand.m
//  KHTC
//
//  Created by Hans Yelek on 5/30/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "CreateAccountCommand.h"
#import "Constants.h"

#define kDidAttemptCreateAccountNotification @"CreateAccountAttemptComplete"

@implementation CreateAccountCommand

@synthesize firstName;
@synthesize lastName;
@synthesize phone;
@synthesize email;
@synthesize password;
@synthesize city;
@synthesize state;

- (void)main
{
    // Format city/state strings: capital first letter, rest lower case
    // Important for avoiding duplicate city/state entries on server side database
    NSString *cityLC = [city lowercaseString];
    NSString *stateLC = [state lowercaseString];
    
    city = [cityLC stringByReplacingCharactersInRange:NSMakeRange(0, 1)
                                           withString:[[cityLC substringToIndex:1] uppercaseString]];
    state = [stateLC stringByReplacingCharactersInRange:NSMakeRange(0, 1)
                                             withString:[[stateLC substringToIndex:1] uppercaseString]];
    
    // Create JSON payload
    //
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:
                             firstName, @"firstName",
                             lastName,  @"lastName",
                             phone,     @"phone",
                             email,     @"email",
                             city,      @"city",
                             state,     @"state",
                             password,  @"password", nil];
    NSError *error = nil;
    NSData *dataPayload = [NSJSONSerialization dataWithJSONObject:payload
                                                          options:NSJSONWritingPrettyPrinted
                                                            error:&error];
    if (error != nil) {
        NSLog(@"Error Creating JSON Object: %@", [error localizedDescription]);
        // error handling here...
        return;
    }
    
    // send new member info to php script
    NSURL *url = [NSURL URLWithString:@"http://atcj.org/KHTC/Account/add_khtc_member.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:dataPayload];
    [request setHTTPMethod:@"POST"];
    
    NSHTTPURLResponse *response = nil;
    NSData *data = [self sendSynchronousRequest:request
                                     response_p:&response
                                          error:&error];
    // DEBUG
    NSLog(@"RESPONSE: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    //abort();
    
    // Check for OS error
    if (error != nil) {
        NSLog(@"OS Error: %@", [error localizedDescription]);
        NSLog(@"HTTP Status Code: %ld", (long)[response statusCode]);
        NSLog(@"HTTP Body: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        // error handling here...
        return;
    }
    
    self.httpStatusCode = [response statusCode];
    NSDictionary *responsePayload = nil;
    
    // Check for HTTP Error
    // OK
    if (self.httpStatusCode == 200) {
        
        responsePayload = [NSJSONSerialization JSONObjectWithData:data
                                                          options:NSJSONReadingMutableContainers
                                                            error:&error];
        // Check for server app error:
        //
        // Error: Attempt to use preexisting email
        if ([responsePayload objectForKey:@"error"]) {
            NSString *errorCode = [[responsePayload objectForKey:@"error"] objectForKey:@"code"];
            NSString *errorMessage = [[responsePayload objectForKey:@"error"] objectForKey:@"message"];
            NSLog(@"Error Code: %@\nError Message: %@", errorCode, errorMessage);
        
            self.status = kFailure;
        }
        // No error: Successful Request
        else {
            
            self.status = kSuccess;
        }
    }
    // HTTP Error
    else {
        NSLog(@"HTTP Error: %ld", (long)[response statusCode]);
        
        self.status = kFailure;
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompleteCreateAccountOpNotification
                                                        object:self
                                                      userInfo:responsePayload];
}

@end
