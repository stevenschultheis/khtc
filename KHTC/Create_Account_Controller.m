//
//  Create_Account_Controller.m
//  KHTC
//
//  Created by Hans Yelek on 8/26/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import <UIKit/UITextField.h>
#import "AppDelegate.h"
#import "Create_Account_Controller.h"
#import "CreateAccountCommand.h"
#import "Inset_TextField.h"
#import "Constants.h"

@interface Create_Account_Controller ()

@end

typedef enum {
    emailCellTag = 1,
    passwordCellTag,
    confirmPassCellTag,
    firstNameCellTag,
    lastNameCellTag,
    phoneCellTag,
    cityCellTag,
    stateCellTag
} CellTags;

@implementation Create_Account_Controller

@synthesize emailField = _emailField;
@synthesize passwordField = _passwordField;
@synthesize confirmPassField = _confirmPassField;
@synthesize firstNameField = _firstNameField;
@synthesize lastNameField = _lastNameField;
@synthesize phoneField = _phoneField;
@synthesize cityField = _cityField;
@synthesize stateField = _stateField;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Create_Account_Controller Methods


#pragma mark - UITextFieldDelegate Protocol Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    UITableView *tableView = self.tableView;
    CellTags cellTag = textField.tag;
    
    /* NOTE: should use member variables instead of tags here */
    /************************/
    switch (cellTag) {
        case emailCellTag:
            [((Inset_TextField *)[tableView viewWithTag:passwordCellTag]) becomeFirstResponder];
            break;
        case passwordCellTag:
            [((Inset_TextField *)[tableView viewWithTag:confirmPassCellTag]) becomeFirstResponder];
            break;
        case confirmPassCellTag:
            [((Inset_TextField *)[tableView viewWithTag:firstNameCellTag]) becomeFirstResponder];
            break;
        case firstNameCellTag:
            [((Inset_TextField *)[tableView viewWithTag:lastNameCellTag]) becomeFirstResponder];
            break;
        case lastNameCellTag:
            [((Inset_TextField *)[tableView viewWithTag:phoneCellTag]) becomeFirstResponder];
            break;
        case phoneCellTag:
            [((Inset_TextField *)[tableView viewWithTag:cityCellTag]) becomeFirstResponder];
            break;
        case cityCellTag:
            [((Inset_TextField *)[tableView viewWithTag:stateCellTag]) becomeFirstResponder];
            break;
        case stateCellTag:
        {
            // form validation here...
            // ...
            //

            
            // send account information to server for account creation
            CreateAccountCommand *command = [[CreateAccountCommand alloc] init];
            [command setEmail:_emailField.text];
            [command setPassword:_passwordField.text];
            [command setFirstName:_firstNameField.text];
            [command setLastName:_lastNameField.text];
            [command setPhone:_phoneField.text];
            [command setCity:_cityField.text];
            [command setState:_stateField.text];
            [command listenForMyCompletion:self
                                  selector:@selector(createAccountCmdDidFinish:)
                          notificationName:kDidCompleteCreateAccountOpNotification];
            [command enqueueOperation];
            break;
        }
    }
    return YES;
}

#pragma mark - UIAlertViewDelegate Protocol Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    /* 
        Need to save user's new profile and push Runs_View_Controller
     */
}

#pragma mark - Notification Methods

// Notification: kDidCompleteCreateAccountOpNotification
// Sent by the CreateAccountCommand object
- (void)createAccountCmdDidFinish:(NSNotification *)notification
{
    CreateAccountCommand *command = [notification object];
    NSLog(@"Command Status: %@", [notification userInfo]);
    
    if (command.status == kSuccess) {
        // if account is successfully created, inform user, pop views
        // from appDelegate's rootVC, and push a new view showing
        // user's current runs
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:@"Success"
                                        message:@"Your account has been created!"
                                       delegate:self
                              cancelButtonTitle:@"ok"
                               otherButtonTitles:nil] show];
        });
    } else {
        /* account creation failed: error handling here ... */
    }
}


#pragma mark - Table view data source


#pragma mark - Table view delegate

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] init];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - IBAction Methods

- (IBAction)createNewAccount:(id)sender
{
    CreateAccountCommand *command = [[CreateAccountCommand alloc] init];
    [command setEmail:_emailField.text];
    [command setPassword:_passwordField.text];
    [command setFirstName:_firstNameField.text];
    [command setLastName:_lastNameField.text];
    [command setPhone:_phoneField.text];
    [command setCity:_cityField.text];
    [command setState:_stateField.text];
    [command listenForMyCompletion:self
                          selector:@selector(createAccountCmdDidFinish:)
                  notificationName:kDidCompleteCreateAccountOpNotification];
    [command enqueueOperation];
}
@end
