//
//  BaseCommand.h
//  KHTC
//
//  Created by Hans Yelek on 5/30/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/********************************************************************************
 *  This class is a modified version of the BaseCommand class from the following:
 *
 *  Professional iOS Network Programming
 *  Chapter 5: Handling Errors
 *  By Jack Cox, Nathan Jones, John Szumski
 *  
 *  ISBN: 978-1-118-36240-2
 
 
 * Every command in this application inherits from this class.
 
    - commands make http requests for information
    - requests are primarily made to atcj.org php scripts or to third party APIs
        such as Google Places API
 *******************************************************************************/

#import <Foundation/Foundation.h>

#define kInProcess 0
#define kFailure  -1
#define kSuccess   1

@interface BaseCommand : NSOperation <NSCopying>

@property (nonatomic, strong)   NSDictionary    *results;
@property (nonatomic, strong)   NSString        *completionNotificationName;
@property (nonatomic)           NSInteger       status;                         // success, failure, in progress
@property (nonatomic)           NSInteger       httpStatusCode;
@property (nonatomic, strong)   NSString        *reason;                        // reason for operation failure
@property (nonatomic, strong)   NSNumber        *requestTimeoutInterval;

// Registers a listener for completion of operation. Listener is notified via NotificationCenter
- (void)listenForMyCompletion:(id)listener selector:(SEL)selector notificationName:(NSString *)name;

// Sends request with calls to turn on network busy spinner
- (NSData *)sendSynchronousRequest:(/*NSMutableURLRequest*/NSURLRequest *)request response_p:(NSHTTPURLResponse **)response_p error:(NSError **)error_p;

// Add receiver to the operation queue
- (void)enqueueOperation;

/****************************
 // Optional Methods to Implement
 //
 
// Removes listener from observing notifications from command
- (void)stopListeningForMyCompletion:(id)listener;

// Send notification of successful operation completion
- (void)sendCompletionNotification;

// Send notification of operation completion failure
- (void)sendCompletionFailureNotification;

- (void)sendNetworkErrorNotification;

// Create request object w/ url and timeout
- (NSMutableURLRequest *)createRequestObject:(NSURL *)url;

- (BOOL)isHttpResponseCodeOK:(NSHTTPURLResponse *)response;

- (BOOL)isNoError:(NSError *)error;

// Convenience function: calls isNoError: and isHttpResponseCodeOK:
- (void)wasCallSuccessful:(NSHTTPURLResponse *)response error:(NSError *)error;
****************************/


@end
