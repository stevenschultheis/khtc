//
//  AppDelegate.m
//  KHTC
//
//  Created by Steven Schultheis on 4/13/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "GoogleAPIConstants.h"
#import "Constants.h"
#import "AmberAlert.h"

#import "Save_RN_Token_Command.h"

#define kDismissAddContactViewController @"DismissAddContactViewController"

@implementation AppDelegate

@synthesize isSignedInWithEmail = _isSignedInWithEmail;
@synthesize isSignedInWithFacebook = _isSignedInWithFacebook;

@synthesize amberAlert = _amberAlert;
@synthesize opQueue;
@synthesize session = _session;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /* Request Permission to Deliver Push Notifications */
    /*
        This request is made every time the application launches.
        See Apple's Local and Push Notification Programming Guide 
    */
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeSound];
    
    //NSLog(@"Font Names for Avenir\n%@", [UIFont fontNamesForFamilyName:@"Avenir"]);
    _amberAlert = [AmberAlert sharedAmberAlert];
    
    self.opQueue = [[NSOperationQueue alloc] init];
    // Override point for customization after application launch.
    [FBLoginView class];
    [GMSServices provideAPIKey:kGoogleMapsAndPlacesAPIKey];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [FBAppCall handleDidBecomeActiveWithSession:self.session];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

    // Close Facebook session
    [self.session close];
    
    // Saves changes in the application's managed object context before the application terminates
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ! [managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use
            // this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error: %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - AppDelegate Methods

// NO LONGER NEEDED 10/27/13
// Returns the ProfileVC for the application. This method is primarily called when the ProfileVC
// needs to push/pop view controllers for log in/out requests.
/*
- (ProfileViewController *)profileViewController
{
    UITabBarController *tbc = (UITabBarController *)self.window.rootViewController;
    return (ProfileViewController *)[[tbc viewControllers] objectAtIndex:0];
}
 */

#pragma mark - Core Data Stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for
// the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"UserProfile" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"UserProfile.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    if (! [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents Directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Facebook Methods

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    // attempt to extract token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:self.session];
}

#pragma mark - Push Notification Delegate Methods

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"Push Notification token: %@", [NSString stringWithFormat:@"%@", deviceToken]);
    /* call script here to register device token with user */
    
    // clean token
    NSString *token = [NSString stringWithFormat:@"%@", deviceToken];
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    Save_RN_Token_Command *saveTokenCmd = [[Save_RN_Token_Command alloc] init];
    /* ID HARD-CODED HERE
     ALSO: CHANGE khtcID type to integer type?
     */
    [saveTokenCmd setKhtcID:@"637"];
    [saveTokenCmd setToken:token];
    [saveTokenCmd listenForMyCompletion:self
                               selector:@selector(saveRMTokenCmdComplete:)
                       notificationName:kDidCompleteRegisterRNTokenOpNotification];
    [saveTokenCmd enqueueOperation];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Failed to register for remote notifications. Error:\n%@", [error userInfo]);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Received Remote Notification!\n%@", userInfo);
}

#pragma mark - Notification Methods

/*
 Called upon completion of Save_RN_Token_Command operation.
 Notification Name: kDidCompleteRegisterRNTokenOpNotification
 */
- (void)saveRMTokenCmdComplete:(NSNotification *)notification
{
    Save_RN_Token_Command *command = [notification object];
    
    if (command.status == kSuccess) {
        // do not notifiy user here...
        NSLog(@"token saved successfully");
    } else {
        // error handling here...
        NSLog(@"Save remote notification token command failed");
        abort();
    }
}

@end
