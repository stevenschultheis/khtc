//
//  GetAmberAlertsCommand.h
//  KHTC
//
//  Created by Hans Yelek on 7/4/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    GetAmberAlertsCommand
 
        - this command request amber alerts within range miles
        - note that the range is converted to meters prior to making the http request: 
            the php script on the server reads for meters, NOT MILES, so this conversion
            needs to remain in place
 
 *******/

#import "BaseCommand.h"

@interface GetAmberAlertsCommand : BaseCommand

@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSNumber *range;   

@end
