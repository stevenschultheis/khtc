//
//  AlertPreferences.m
//  KHTC
//
//  Created by Hans Yelek on 8/20/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "AlertPreferences.h"


@implementation AlertPreferences

@dynamic locationLat;
@dynamic locationLong;
@dynamic locationName;
@dynamic searchRadius;

@end
