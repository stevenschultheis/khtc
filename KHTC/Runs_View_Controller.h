//
//  Runs_View_Controller.h
//  KHTC
//
//  Created by Hans Yelek on 9/4/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    Runs_View_Controller
 
        - this controller has a view on storyboard
 
        - displays either a list of the user's runs, or a list of data for
            a single run if the user is assigned only one
 
        NOTE:
            A remote notification push is currently linked to the "driver dispatch" button.
            (see IBAction method below)
            - this was for testing purposes in order to get remote notifications functioning,
                but is not proper for the final app.
            - The driver dispatch button is meant to push another view controller, perhaps with 
                a table view of the remote notification types the user/driver might wish to send.
                * Example Remote Notification Types:
                    1) began run leg
                    2) arrived at run destination
                    3) late on arrival
                    4) estimated arrival time
                    etc.
                - selection of one of these types should push a remote notification with notification
                    body tailored to the particular type of notification
 
 *******/

#import <UIKit/UIKit.h>

@interface Runs_View_Controller : UITableViewController
{
    BOOL assignedOneRun;
    
    NSArray *runCoordinator;
    NSArray *drivers;
    NSArray *pets;
}

@property (nonatomic, strong) NSDictionary *runs;   // dictionary of the runs assigned to the driver

- (IBAction)pushNotificationTest:(id)sender;


@end
