//
//  BaseCommand.m
//  KHTC
//
//  Created by Hans Yelek on 5/30/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "BaseCommand.h"
#import "AppDelegate.h"
#import "Constants.h"

@implementation BaseCommand

- (void)listenForMyCompletion:(id)listener selector:(SEL)selector notificationName:(NSString *)name
{
    // Confirm that listener responds to selector.
    if ([listener respondsToSelector:selector]) {
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        
        // make sure listener gets only 1 notification
        [nc removeObserver:listener name:name object:nil];
        // Add object as listener
        [nc addObserver:listener selector:selector name:name object:nil];
    } else {
        NSLog(@"WARNING: Listener does not respond to selector and is not observing notification!");
    }
}

- (NSData *)sendSynchronousRequest:(/*NSMutableURLRequest*/NSURLRequest *)request response_p:(NSHTTPURLResponse *__autoreleasing *)response_p error:(NSError *__autoreleasing *)error_p
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:response_p
                                                     error:error_p];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    return data;
}

- (void)enqueueOperation
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate.opQueue addOperation:self];
    return;
}

@end
