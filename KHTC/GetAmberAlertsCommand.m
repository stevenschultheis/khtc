//
//  GetAmberAlertsCommand.m
//  KHTC
//
//  Created by Hans Yelek on 7/4/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "GetAmberAlertsCommand.h"
#import "Constants.h"

#define kMetersInOneMile 1609.34
#define kPathToAlertsNearCoordinateScript @"http://atcj.org/KHTC/alertsNearCoordinate.php"

#pragma mark - C Functions

double milesToMeters(double miles)
{
    return miles * kMetersInOneMile;
}

@implementation GetAmberAlertsCommand

@synthesize latitude = _latitude;
@synthesize longitude = _longitude;
@synthesize range = _range;

#pragma mark - Accessor Methods

- (void)setRange:(NSNumber *)range
{
    // Argument is assumed given in miles.
    // Convert to meters before setting _range
    double miles = [range doubleValue];
    double meters = milesToMeters(miles);
    
    _range = [[NSNumber alloc] initWithDouble:meters];
}


- (void)main
{
    // Create JSON Payload
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:
                             _latitude, @"latitude",
                             _longitude, @"longitude",
                             _range, @"radius",
                             nil];
    
    NSError *error = nil;
    NSData *dataPayload = [NSJSONSerialization dataWithJSONObject:payload
                                                          options:NSJSONWritingPrettyPrinted
                                                            error:&error];
    if (error != nil) {
        NSLog(@"Error creating JSON object: %@", [error localizedDescription]);
        // error handling here...
        abort();
    }
    
    // Send coordinates to alertsNearCoordinate.php
    NSURL *url = [NSURL URLWithString:kPathToAlertsNearCoordinateScript];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:dataPayload];
    
    NSHTTPURLResponse *response = nil;
    NSData *data = [self sendSynchronousRequest:request
                                     response_p:&response
                                          error:&error];
    // Check for OS Error
    if (error != nil) {
        NSLog(@"OS Error: %@", [error localizedDescription]);
        // error handling here...
        abort();
    }
    //NSLog(@"Data: %@", [data description]);
    self.httpStatusCode = [response statusCode];
    NSDictionary *responsePayload;
    
    // Check for HTTP Error
    // OK
    if (self.httpStatusCode == 200) {
        responsePayload = [NSJSONSerialization JSONObjectWithData:data
                                                          options:NSJSONReadingMutableContainers
                                                            error:&error];
        if (error != nil) {
            NSLog(@"%@: Error creating JSON object", NSStringFromSelector(_cmd));
            NSLog(@"Error: %@", [error localizedDescription]);
            NSLog(@"Error Desc: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            // error handling here...
            abort();
        }
        /////NSLog(@"JSON Payload:\n%@", responsePayload);
        // Check for server app error
        if ([responsePayload objectForKey:@"error"]) {
            NSLog(@"Server Error: %@", [responsePayload objectForKey:@"error"]);
            
            self.status = kFailure;
        }
        // No error: Successful Request
        else {
            self.status = kSuccess;
           // NSLog(@"Success Data Test: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        }
    }
    // HTTP Error
    else {
        NSLog(@"HTTP Error: %ld", (long)[response statusCode]);
        self.status = kFailure;
        responsePayload = [NSJSONSerialization JSONObjectWithData:data
                                                          options:NSJSONReadingMutableContainers
                                                            error:&error];
        //NSLog(@"Error Desc: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompleteGetAmberAlertsOpNotification
                                                        object:self
                                                      userInfo:responsePayload];
}

@end
