//
//  CurrentProfile.h
//  KHTC
//
//  Created by Hans Yelek on 6/16/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
    
    CurrentProfile
 
        - the current profile is made to hold the user's current profile
 
        - user profiles to this point have not been saved or used much in
            a productive manner
        - saving user profiles would have benefits.
            - for example, saving the user's password locally can save the user from 
                having to make an http request to the database for password matching
                while logging in
 
 *******/

#import <Foundation/Foundation.h>

@class FacebookUserProfile;
@class EmailUserProfile;

@interface CurrentProfile : NSObject
{

}

@property (nonatomic, strong) FacebookUserProfile *facebookProfile;
@property (nonatomic, strong) EmailUserProfile *emailProfile;

@property (nonatomic, assign) BOOL loggedInWithFacebook;
@property (nonatomic, assign) BOOL loggedInWithEmail;

+ (CurrentProfile *)sharedCurrentProfile;

// If logged in with Facebook, these methods return values corresponding to
// facebookProfile.
// If logged in with email, methods return values from emailProfile.
- (NSString *)firstName;
- (NSString *)lastName;
- (NSString *)email;
- (NSString *)city;
- (NSString *)state;
- (NSString *)password;
- (NSString *)phone;
- (UIImage *)profileImage;

@end
