//
//  CreateAccountCommand.h
//  KHTC
//
//  Created by Hans Yelek on 5/30/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "BaseCommand.h"

/*
    This command saves a new user's account information (see properties below)
    to the atcj.org database.
 */

@interface CreateAccountCommand : BaseCommand

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *state;

@end
