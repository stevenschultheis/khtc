//
//  GooglePlacesAPICommand.m
//  KHTC
//
//  Created by Hans Yelek on 7/4/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "GooglePlacesAPICommand.h"
#import "GoogleAPIConstants.h"
#import "Constants.h"

@implementation GooglePlacesAPICommand

@synthesize place = _place;

- (void)main
{
    NSString *baseURLString = kGooglePlacesTextSearchAPIPath;
    NSString *parameters = [NSString stringWithFormat:@"query=%@&sensor=false&key=%@",
                            _place, kGoogleMapsAndPlacesAPIKey];
    NSString *urlString = [[NSString stringWithFormat:@"%@%@", baseURLString, parameters] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    if (url == nil) {
        NSLog(@"error creating Google Places Text Search API URL");
        // error handling here...
        abort();
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    
    NSData *responseData = [self sendSynchronousRequest:request
                                             response_p:&response
                                                  error:&error];
    
    // Check for OS error
    if (error != nil) {
        NSLog(@"OS Error: %@", [error localizedDescription]);
        NSLog(@"HTTP Status Code: %ld", (long)[response statusCode]);
        NSLog(@"HTTP Body: %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        // error handling here...
        return;
    }
    
    self.httpStatusCode = [response statusCode];
    NSDictionary *responsePayload = nil;
    
    // Check for HTTP Error
    // OK
    if (self.httpStatusCode == 200) {
        responsePayload = [NSJSONSerialization JSONObjectWithData:responseData
                                                          options:NSJSONReadingMutableContainers
                                                            error:&error];
        if (error != nil) {
            NSLog(@"Error converting data to JSON object");
            // error handling here...
        }
        
        self.status = kSuccess;
    }
    // HTTP Error
    else {
        self.status = kFailure;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompleteGooglePlacesTextSearchOpNotification
                                                        object:self
                                                      userInfo:responsePayload];
    
}

@end
