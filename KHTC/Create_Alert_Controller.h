//
//  Create_Alert_Controller.h
//  KHTC
//
//  Created by Hans Yelek on 8/21/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    Create_Alert_Controller 
 
        - (this controller has a view on storyboard)
 
    *   This controller allows the user to create a new missing animal alert. When the
    *   user saves the alert, the data is sent to the atcj.org server and saved in the
    *   database there.
 
 
 *******/

#import <UIKit/UIKit.h>
#import "LastLocationController.h"

@class CameraOrRollAlert;
@class AmberAlert;

@interface Create_Alert_Controller : UITableViewController <UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, LastLocationControllerDelegate>
{
    
}

@property (strong, nonatomic) AmberAlert *amberAlert;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *breedTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *animalImage;

@property (strong, nonatomic) IBOutlet CameraOrRollAlert *cameraAlertView;


- (IBAction)showCameraOrRollAlertView:(id)sender;
- (IBAction)newPhoto:(id)sender;
- (IBAction)photoFromCameraRoll:(id)sender;
- (IBAction)dismissCameraOrRollView:(id)sender;
- (IBAction)createNewAlert:(id)sender;



@end
