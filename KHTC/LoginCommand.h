//
//  LoginCommand.h
//  KHTC
//
//  Created by Hans Yelek on 5/30/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
    
    LoginCommand
 
        - takes a user's email and password and checks for them in a single row in the
            atcj.org database
        - failure to find a matching row results in login failure
 
    NOTE:
        - the passwords on the database are not currently encrypted; this is NOT OK!
            - passwords MUST be encrypted with RSA or some other encryption method prior
                to the app's going public
            - as such, the password property (see below) is not encrypted before being
                sent to the php script
 
 *******/

#import "BaseCommand.h"

@interface LoginCommand : BaseCommand

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;

@end
