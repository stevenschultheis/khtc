//
//  SignIn_View_Controller.h
//  KHTC
//
//  Created by Hans Yelek on 8/23/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    SignIn_View_Controller
 
        (controller on storyboard)
 
        * controller gives user option to sign in or to create new account
 
 *******/

#import <UIKit/UIKit.h>

@interface SignIn_View_Controller : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *signInBtn;


- (IBAction)signIn:(id)sender;

@end
