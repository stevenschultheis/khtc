//
//  AppDelegate.h
//  KHTC
//
//  Created by Steven Schultheis on 4/13/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*
 
 TO DO LIST
 
 - location names for alerts not always correct
    -> turn off autocomplete for textfield in LastLocationController
 - round image corners
 - setting amber alert location to CU Boulder results in NO coordinates!
 - include a default alert image if user does not include one
 
 - issue with search_for_member.php script when searching for member
    - leading space prior to % in LIKE clause (ex: "hans %") appropriate only when two or
      more strings are in the cell: i.e. "hans yelek"
    - search fails when only one trimmed string in cell
        - example: the clause LIKE 'hans %' will NOT catch "hans"
 
 - encrypt passwords before saving to database

 
 */

// DOUBLE ENCORE
// http://subhb.org

//https://developer.apple.com/devforums
//
/**********************************************************************************************
 * ____________________________________________________________________________________________
 *
 * Maps Research:
 *
 * Apple Documentation:
 * - Location Awareness Programming Guide
 * - Core Location Framework Reference
 *
 * Apps:
 *  - Find My Friends
 *  - Google Latitude
 *  - Waze
 *
 * Links:
 *  - http://stackoverflow.com/questions/11354546/ios-api-for-find-my-friends-functionality
 *  - http://stackoverflow.com/questions/13394575/im-trying-to-make-an-iphone-app-similar-to-find-my-friends-how-do-i-find-the-loc?lq=1
 *  - http://www.linkedin.com/groups/How-make-app-simliar-Apples-72283.S.113130561
 *
 *  - Google Maps API
 *  - http://stackoverflow.com/questions/15076738/google-maps-ios-sdk-search-for-places
 *      - Google Places API
 * ____________________________________________________________________________________________
 *
 * Unique Identifiers:
 *
 * Links:
 *  - http://stackoverflow.com/questions/7273014/ios-unique-user-identifier
 * ____________________________________________________________________________________________
 *
 * User Interface:
 *
 * Apple:
 *  - iOS Human Interface Guidelines
 *  - View Programming Guide for iOS
 *
 * Links:
 *  - http://www.mobile-patterns.com
 *  - http://www.computerarts.co.uk/features/50-tips-designing-brilliant-ios-apps
 *  - http://techcrunch.com/2013/06/10/apple-publishes-ios-7-transition-guide-to-help-developers-adopt-flat-design/
 *  - https://developer.apple.com/library/prerelease/ios/documentation/UserExperience/Conceptual/TransitionGuide/index.html
 *  - http://bloomwebdesign.net/2013/05/flat-ui-design/
 *
 * Websites:
 *  - Creattica
 *      - http://creattica.com/mobile/flat-web-ui-kit/93027
 *  - Dribbble
 *      - http://dribbble.com/shots/1052414-FREE-PSDs-iGravity-Screen-Layers-Up-to-4-in-1?list=users
 *  - TapFancy
 *  - iOSpirations
 *  - iOS App Roundups
 *  - Pinterest
 *      - http://pinterest.com/warmarc/flat-ui-design/
 *      - http://pinterest.com/pin/135671007497182937/
 *      - http://pinterest.com/pin/135671007497175271/
 *  - Google
 *      - Search Images: "flat form user interface"
 * ____________________________________________________________________________________________
 *
 * Gradients to UITableViewCells / Customization
 * 
 * Links:
 *  - http://stackoverflow.com/questions/6944478/how-to-make-gradient-background-in-uitableviewcell-in-ios
 *  - http://www.raywenderlich.com/32283/core-graphics-tutorial-lines-rectangles-and-gradients
 *
 *  - http://stackoverflow.com/questions/281515/how-to-customize-the-background-color-of-a-uitableviewcell
 * ____________________________________________________________________________________________
 *
 * UIElement Customization
 *
 * Links:
 *  - http://stackoverflow.com/questions/8774531/change-uinavigationbar-font-properties
 *  - http://www.raywenderlich.com/4344/user-interface-customization-in-ios-5
 * ____________________________________________________________________________________________
 *
 * UITableView Section Animation
 *
 *  Apple Example Code:
 *  - 'Table View Animations and Gestures'
 * ____________________________________________________________________________________________
 *
 * Camera / UIImage
 *
 * Apps:
 *  - Brewster
 *
 * UIImage Orientation
 *
 *  Links:
 *  - http://stackoverflow.com/questions/3853553/iphone-camera-pictures-from-uiimagepickercontroller-are-sideways
 *  - http://stackoverflow.com/questions/1282830/uiimagepickercontroller-uiimage-memory-and-more
 *
 *  Downloading Images From a Database
 *  - http://stackoverflow.com/questions/15878873/retrieving-images-form-database-is-taking-too-long-in-iphone
 * ____________________________________________________________________________________________
 *
 * Load View Using Nib
 *
 *  Links:
 *   - http://stackoverflow.com/questions/863321/iphone-how-to-load-a-view-using-a-nib-file-created-with-interface-builder
 *   - http://stackoverflow.com/questions/5354653/adding-a-custom-subview-created-in-a-xib-to-a-view-controllers-view-what-am
 * ____________________________________________________________________________________________
 *
 * Forms
 *
 * Links:
 *  - http://stackoverflow.com/questions/13552422/existing-ios-form-framework
 *  - https://github.com/ittybittydude/IBAForms
 *  - https://github.com/jasperblues/iBureaucrat
 *  - http://stackoverflow.com/questions/7202647/what-are-the-best-practices-for-implementing-form-in-ios
 * ____________________________________________________________________________________________
 *
 * PHP
 *
 * Distance Between Two Geopoints
 *
 * Haversine Formula
 * Links:
 *  - http://www.inkplant.com/code/calculate-the-distance-between-two-points.php
 *  - http://stackoverflow.com/questions/10053358/measuring-the-distance-between-two-coordinates-in-php
 *  - http://en.wikipedia.org/wiki/Haversine_formula
 * ____________________________________________________________________________________________
 *
 * Converting Binary to Base64 for JSON Payload
 *
 * Books:
 *  - Professional iOS Network Programming (ISBN 978-1-118-36240-2)
 *      - page 253
 *
 * Links:
 *  - http://stackoverflow.com/questions/1443158/binary-data-in-json-string-something-better-than-base64
 * ____________________________________________________________________________________________
 *
 * Errors
 *  ARMv7 errors:
 *  
 *  Links:
 *   - http://stackoverflow.com/questions/12402092/file-is-universal-three-slices-but-it-does-not-contain-an-armv7-s-slice-err
 * ____________________________________________________________________________________________
 *
 * Looping Through PDOStatement Results After Database SELECT Statement
 *
 * Links:
 *  - http://stackoverflow.com/questions/159924/how-do-i-loop-through-a-mysql-query-via-pdo-in-php
 *  - http://www.php.net/manual/en/pdostatement.fetch.php
 **********************************************************************************************/

/**************************************************
 *
 * UIScrollView resizing after - (void)viewWillAppear:
 * see http://stackoverflow.com/questions/7178789/uiscrollview-wont-scroll-even-when-i-set-content-size
 *
 **************************************************/

/* --------------------------------------------------
 
 Round Corners of Buttons
    - stackoverflow.com/questions/5047818/how-to-round-the-corners-of-a-button
    - (uses QuartzCore framework)
 
 Round Corners of Images
    - stackoverflow: "How to mask a square image into an image with round corners in the iPhone SDK?"
    - (uses the CoreGraphics framework)
 
 Using QuartzCore for Drawing
    - www.techotopia.com/index.php/An_iOS_4_iPhone_Graphics_Drawing_Tutorial_using_Quartz_2D
 
 Views with Rounded Corners
    - stackoverflow.com/questions/1509547/uiview-with-rounded-corners
 
 Custom Views for GMSMarkers
    - stackoverflow.com/questions/15537206/how-can-i-add-a-button-to-google-maps-marker-info-window-on-ios
 
 Canceling and Performing Your Own Segues
    - stackoverflow.com/questions/8993341/ios-segue-cancel
 
 PHP Cannot Pass Parameter #__ By Reference
    - stackoverflow.com/questions/13105373/php-error-cannot-pass-parameter-2-by-reference
 
 Text Insets for UITextFields
    - stackoverflow.com/questions/2694411/text-inset-for-uitextfield
 
 Convert SQLite Time Stamps to Local Time
    - stackoverflow/questions/381371/sqlite-current-timestamp-is-in-gmt-not-the-timezone-of-the-machine
 
 Get Last Record in SQLite Table
    - stackoverflow/questions/11766297/retrieve-last-record-in-sqlite-table-again
 
 Google Maps JavaScript API 'place_changed' event
    - developers.google.com/maps/documentation/javascript/places
    - PlaceResult object
    - * Separate latitude and longitude coordinates from PlaceResult.location object:
        - stackoverflow.com/questions/10647389/placeresult-object-returns-latitude-longitude-as-object-not-sure-how-to-get-the
 
 JavaScript JSON.stringify -> arrays are empty
    - stackoverflow/questions/3602713/jquery-json-stringify-array-is-empty
        - must convert arrays to objects
            - stackoverflow/questions/4215737/convert-array-to-object
 
 ---------------------------------------------------*/
 

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <GoogleMaps/GoogleMaps.h>

@class AmberAlert;
@class ProfileViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic) BOOL isSignedInWithEmail;
@property (nonatomic) BOOL isSignedInWithFacebook;


@property (strong, nonatomic) AmberAlert *amberAlert;
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) NSOperationQueue *opQueue;

// Facebook session object
@property (strong, nonatomic) FBSession *session;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
//- (ProfileViewController *)profileViewController;


@end