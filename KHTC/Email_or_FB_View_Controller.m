//
//  Email_or_FB_View_Controller.m
//  KHTC
//
//  Created by Hans Yelek on 8/30/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "Email_or_FB_View_Controller.h"

@interface Email_or_FB_View_Controller ()

@end

@implementation Email_or_FB_View_Controller

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _fbLoginController= [[FacebookLoginController alloc] init];
    
    /**  set FBLoginView  **/
    FBLoginView *loginView = [[FBLoginView alloc] initWithReadPermissions:@[@"basic_info", @"email"]];
    
    loginView.frame = CGRectMake(37, 298, 249, 44);
    loginView.delegate = _fbLoginController;
    [self.view addSubview:loginView];
    
    // *** //
    // Customizing the FBLoginView by iterating through subview complements of stackoverflow user ozba
    // see: stackoverflow.com/questions/12280850/how-to-customize-fbloginview
    for (id obj in loginView.subviews) {
        /* customize background */
        if ([obj isKindOfClass:[UIButton class]]) {
            UIButton *loginBtn = obj;
            UIImage *loginImg = [UIImage imageNamed:@"white-button-border_03.png"];
            [loginBtn setBackgroundImage:loginImg forState:UIControlStateNormal];
        }
        /* customize label */
        else if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *loginLabel = obj;
            [loginLabel setText:@"facebook"];
            [loginLabel setTextColor:[UIColor colorWithRed:183/255.0 green:206/255.0 blue:1.0 alpha:1.0]];
            [loginLabel setFont:[UIFont fontWithName:@"Avenir-Black" size:17.0]];
            [loginLabel setTextAlignment:NSTextAlignmentCenter];
            [loginLabel setFrame:CGRectMake(0.0, 0.0, 249.0, 44.0)];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
