//
//  GooglePlacesAutoCompleteCommand.m
//  KHTC
//
//  Created by Hans Yelek on 7/3/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "GooglePlacesAutoCompleteCommand.h"
#import "GoogleAPIConstants.h"
#import "Constants.h"

@implementation GooglePlacesAutoCompleteCommand

@synthesize place = _place;

- (void)main
{
    // send Google Places Autocomplete API Request
    NSString *baseURLString = kGooglePlacesAutoCompleteAPIPath;
    NSString *parameters = [NSString stringWithFormat:@"input=%@&sensor=false&key=%@",
                            _place, kGoogleMapsAndPlacesAPIKey];
    NSString *urlString = [[NSString stringWithFormat:@"%@%@", baseURLString, parameters]
                           stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    if (url == nil) {
        NSLog (@"Error creating url");
        // error handling here...
        abort();
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:20.0];
    
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSData *jsonResponseData = [self sendSynchronousRequest:request
                                                 response_p:&response
                                                      error:&error];
    // Check for OS Error
    if (error != nil) {
        NSLog(@"OS Error: %@", [error localizedDescription]);
        // error handling here...
        abort();
    }
    
    self.httpStatusCode = [response statusCode];
    NSDictionary *responsePayload = nil;
    
    // Check for HTTP Error
    // OK
    if (self.httpStatusCode == 200) {
        responsePayload = [NSJSONSerialization JSONObjectWithData:jsonResponseData
                                                          options:NSJSONReadingMutableContainers
                                                            error:&error];
        self.status = kSuccess;
    }
    // HTTP Error
    else {
        // as this is just the autocomplete api, perhaps it's unwise to alert the
        // user whenever this fails.
        // perhaps could keep track of number of consecutive failures, and stop making
        // request after so many.
        self.status = kFailure;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompleteGooglePlacesAutoCompleteOpNotification
                                                        object:self
                                                      userInfo:responsePayload];
}

@end