//
//  LoginCommand.m
//  KHTC
//
//  Created by Hans Yelek on 5/30/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "LoginCommand.h"
#import "Constants.h"

//#define kDidAttemptLoginNotification @"LoginAttemptComplete"

@implementation LoginCommand

@synthesize email;
@synthesize password;

- (void)main
{
    // create JSON payload
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          email, @"email",
                          password, @"password", nil];
    
    NSError *error = nil;
    NSData *payload = [NSJSONSerialization dataWithJSONObject:dict
                                                      options:NSJSONWritingPrettyPrinted
                                                        error:&error];
    if (error != nil) {
        NSLog(@"Error creating JSON payload: %@", [error localizedDescription]);
        // error handling here...
        return;
    }
    
    // Create and send URL request
    NSURL *url = [NSURL URLWithString:@"http://atcj.org/KHTC/memberLogin.php"];

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:payload];
    
    NSHTTPURLResponse *response = nil;
    //error = nil;
    
    
    NSData *data = [self sendSynchronousRequest:request
                      response_p:&response
                           error:&error];
    
    
    // Check for OS error
    //
    if (error != nil) {
        NSLog(@"OS Error Making URL Request: %@", [error localizedDescription]);
        NSLog(@"HTTP Status Code: %ld", (long)[response statusCode]);
        NSLog(@"HTTP Body: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        // error handling here...
        return;
    }
    
    NSDictionary *responsePayload = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:NSJSONReadingMutableContainers
                                                                      error:&error];
    
    //error = nil;
    self.httpStatusCode = [response statusCode];
    
    // Check for HTTP Error
    // OK
    if (self.httpStatusCode == 200) {
        
        self.status = kSuccess;
    }
    // HTTP Error
    else {
        
        self.status = kFailure;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompleteSignInOpNotification
                                                        object:self
                                                      userInfo:responsePayload];
}

@end
