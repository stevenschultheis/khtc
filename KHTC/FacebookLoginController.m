//
//  FacebookLoginController.m
//  KHTC
//
//  Created by Hans Yelek on 6/16/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "FacebookLoginController.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "CurrentProfile.h"
#import "FacebookUserProfile.h"
#import "Constants.h"

@interface FacebookLoginController ()

@end

@implementation FacebookLoginController

@synthesize facebookLogin = _facebookLogin;

@synthesize fetchedFBProfilesController = _fetchedFBProfilesController;
@synthesize fetchedEmailProfilesController = _fetchedEmailProfilesController;
@synthesize managedObjectContext = _managedObjectContext;

@synthesize facebookUser = _facebookUser;

- (id)init
{
    self = [super init];
    if (self) {
        _facebookLogin = NO;    // Sets to YES if user logs in with Facebook
        
        // Set managed object context
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        _managedObjectContext = appDelegate.managedObjectContext;
        
    }
    return self;
}

/*
 *  Creates a new Facebook profile with basic and email properties:
 *  - First Name
 *  - Last Name
 *  - Email
 *  - City
 *  - State
 */
- (FacebookUserProfile *)createNewFBProfileWithBasicAndEmailFromUser:(id<FBGraphUser>)user
{
    FacebookUserProfile *newFBProfile =
        [NSEntityDescription insertNewObjectForEntityForName:@"FacebookUserProfile"
                                      inManagedObjectContext:_managedObjectContext];
    
    NSString *location = user.location.name;
    NSArray *cityAndState = [location componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@", "]];
    
    [newFBProfile setFacebookID:user.id];
    [newFBProfile setFirstName:user.first_name];
    [newFBProfile setLastName:user.last_name];
    [newFBProfile setEmail:[user objectForKey:@"email"]];
    [newFBProfile setCity:[cityAndState objectAtIndex:0]];
    [newFBProfile setState:[cityAndState objectAtIndex:2]];
    
    // The image is created in the ProfileVC using a FBProfilePictureView and initializing it
    // with the Facebook profile id
    
    return newFBProfile;
}

#pragma mark - FBLoginViewDelegate Methods

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    NSLog(@"%@ called", NSStringFromSelector(_cmd));
    
    _facebookLogin = YES;
}

/////////////////////////////////////////////////////////////////////////////
// User has now logged in with Facebook, and user info is fetched.
// 1) Check to see if user.id exists in any of the saved Facebook profiles
//   a) - If YES, ProfileVC dismisses all login/account creation controllers
//      - and displays data from logged-in Facebook profile
//      _____________________________________________________________________
//
//   b) - If NO, then user is logging in with a new Facebook profile.
//      - Push a NewAccountVC from this LoginVC with alert to finish account
//      - creation.
//      - Upon successful account creation in NewAccountVC, the ProfileVC
//      - dismisses all login/account VCs as usual
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user
{
    NSLog(@"%@ called", NSStringFromSelector(_cmd));
    //NSLog(@"USER in delegate method:\n%@", user);
    // NOTE: This method is being called twice after the user hits the FBLoginView. This causes
    // a bug in the program unless user is checked against self.facebookUser. If the two hold the
    // same data, the method has been called twice. For now, check if the user id is equivalent
    // self.facebookUser's id. If so, immediately return from this method.
    // *
    // This issue has been brought forth on stack overflow. As of 06/18/13, no solution/explanation
    // has been given. The link is below:
    // http://stackoverflow.com/questions/13156998/facebook-loginviewfetcheduserinfo-is-called-twice
    // Also see
    // http://stackoverflow.com/questions/7572107/ios-facebook-connect-not-working-when-user-has-fbs-app-installed
    //
    //
    // Check if user already exists in current user
    CurrentProfile *currentProfile = [CurrentProfile sharedCurrentProfile];
    NSLog(@"\ncurrent profile: %@\nuser.id: %@", currentProfile.facebookProfile.facebookID, user.id);
    if (([currentProfile.facebookProfile.facebookID isEqualToString:user.id])) {
        NSLog(@"%@ CALLED TWICE", NSStringFromSelector(_cmd));
        return;
    }
    
    _facebookLogin = YES;
    _facebookUser = user;
    // send notification to ProfileViewController that Facebook user info received
    // [[NSNotificationCenter defaultCenter] postNotificationName:kDidReceiveFBUserInfoNotification
    //                                                     object:user];
    /////////////////////////////////////////////////////////////////////////////
    // ^^^^^^^^^^^^^^^^^^^^^^
    // MAY DELETE ABOVE
    
    // Request saved Facebook user profiles
    NSError *error = nil;
    if (! [self.fetchedFBProfilesController performFetch:&error]) {
        // Add error handling here ...
        NSLog(@"Unresolved fetch error: %@, %@", error, [error userInfo]);
        abort();
    }
    NSArray *facebookUserProfiles = [self.fetchedFBProfilesController fetchedObjects];
    
    // Check if user.id exists in a profile in facebookUserProfiles
    BOOL facebookProfileExists = NO;
    
    for (FacebookUserProfile *fbProfile in facebookUserProfiles) {
        if ([user.id isEqualToString:fbProfile.facebookID]) {
            facebookProfileExists = YES;
            // set the currently logged in profile
            CurrentProfile *currentProfile = [CurrentProfile sharedCurrentProfile];
            [currentProfile setFacebookProfile:fbProfile];
 //////////////////

            
            /////
            // User has logged in with a pre-existing Facebook profile. Send a notification to
            // the ProfileVC that the user logged in to an existing account. ProfileVC will set its
            // BOOL variable isLoggedIn to YES and dismiss the login/create account VCs
 //           [pvc dismissViewControllerAnimated:YES completion:nil];
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:kFBUserDidLogInWithExistingAccountNotification
                           object:self];
            
            break;
        }
    }
    // If the Facebook profile does not exist, push NewAccountVC and request
    // completion of account creation
    // Notification is received by LoginVC and HomeVC, both of which hold FBLoginViews
    // Also, create new FacebookUserProfile object and set the basic and email properties
    if (! facebookProfileExists) {
        FacebookUserProfile *newFBProfile = [self createNewFBProfileWithBasicAndEmailFromUser:user];
        CurrentProfile *currentProfile = [CurrentProfile sharedCurrentProfile];
        [currentProfile setFacebookProfile:newFBProfile];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidStartFBAccountCreationNotification object:self];
    }
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView
{
    /*
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    ProfileViewController *pvc = [appDelegate profileViewController];
    [pvc dismissViewControllerAnimated:NO completion:nil];
     */
    // Remove the Facebook profile from the current profile
    CurrentProfile *currentProfile = [CurrentProfile sharedCurrentProfile];
    currentProfile.facebookProfile = nil;
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:kUserDidSignOutOfFBNotification
    //                                                    object:self];
    // ^^^^^^^^^^^ Replaced with the more generic kUserDidSignOutNotification below:
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserDidSignOutNotification
                                                        object:self];
}

- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error
{
    NSLog(@"%@ called", NSStringFromSelector(_cmd));
    NSLog(@"FBLoginViewError: %@\nERROR USER INFO %@\nERROR DESCRIPTION %@",
          error, [error userInfo], [error description]);
}

#pragma mark - Core Data Stack

// WHY IS THIS IN THE FACEBOOK LOGIN CONTROLLER? MOVE IT
//
//
- (NSFetchedResultsController *)fetchedEmailProfilesController
{
    if (_fetchedEmailProfilesController != nil) {
        return _fetchedEmailProfilesController;
    }
    
    /*
     Set up the fetched results controller
     */
    // Create the fetch request for the EmailUserProfile entity
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *emailUserProfile = [NSEntityDescription entityForName:@"EmailUserProfile" inManagedObjectContext:_managedObjectContext];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"email" ascending:YES];
    
    [request setEntity:emailUserProfile];
    [request setSortDescriptors:@[sortDescriptor]];
    [request setFetchBatchSize:10];
    
    // Create the fetched results controller
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    frc.delegate = self;
    self.fetchedEmailProfilesController = frc;
    
    return _fetchedEmailProfilesController;
}

- (NSFetchedResultsController *)fetchedFBProfilesController
{
    if (_fetchedFBProfilesController != nil) {
        return _fetchedFBProfilesController;
    }
    
    // Create the fetch request for the FacebookUserProfile entity
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *facebookUserProfile = [NSEntityDescription entityForName:@"FacebookUserProfile" inManagedObjectContext:_managedObjectContext];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"facebookID" ascending:YES];
    
    [request setEntity:facebookUserProfile];
    [request setSortDescriptors:@[sortDescriptor]];
    [request setFetchBatchSize:10];
    
    // Create the fetched results controller
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    
    frc.delegate = self;
    self.fetchedFBProfilesController = frc;
    
    return _fetchedFBProfilesController;
}

#pragma mark - NSFetchedResultsControllerDelegate Protocol Methods

@end
