//
//  Home_View_Controller.h
//  KHTC
//
//  Created by Hans Yelek on 8/23/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    Home_View_Controller
 
        * (this controller has a view on the storyboard)
 
        * This is the first controller the user will see when opening the application
            for the first time. From here the user has the option to view missing animal
            alerts or to view personal transport/run information.
 
        * When the user selects the "transport" button for the first time, he/she will be 
            requested to create a new account or log in before run information is loaded.
 
            - users should be able to log in with either email or Facebook
 
        IMPORTANT NOTE:
 
            - in the viewTransportData: method, the khtcID is CURRENTLY HARDCODED as 637.
                This was for development purposes
            - for the public app, this number has to be obtained from the user's profile
                -this profile needs to be saved locally so that this unique id and other
                information is always available to the application
 
                - classes for this have already been made: see files in 1.0/Models/Core Data/
                    in the Navigator panel to the left

 
 *******/

#import <UIKit/UIKit.h>

@interface Home_View_Controller : UIViewController

@property (nonatomic, strong) NSDictionary *runs;

- (IBAction)viewTransportData:(id)sender;

@end
