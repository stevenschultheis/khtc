//
//  Find_Member_View_Controller.m
//  KHTC
//
//  Created by Hans Yelek on 8/23/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*
 TO DO: 
    - trim text field data before sending to command object
 */

#import "Find_Member_View_Controller.h"
#import "Create_Account_Controller.h"
#import "Search_Member_Command.h"
#import "Save_Password_Command.h"
#import "Constants.h"

#define kPushCreateAccountVCSegueID @"pushCreateAccountVC"

/* Alert View Titles */
#define kSuccessAlertTitle @"Success"
#define kMemberNotFoundAlertTitle @"Email/Name Not Found"
#define kDifferentPasswordsAlertTitle @"Passwords Do Not Match"
#define kAccountCreatedAlertTitle @"Account Created!"

@interface Find_Member_View_Controller ()

@end

typedef enum {
    emailCellTag = 1,
    firstNameCellTag
} cellTags;

@implementation Find_Member_View_Controller

@synthesize topImageView = _topImageView;
@synthesize bottomImageView = _bottomImageView;
@synthesize userPrompt = _userPrompt;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate Protocol Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField.tag == emailCellTag) {
        UITextField *nameField = (UITextField *)[self.view viewWithTag:firstNameCellTag];
        [nameField becomeFirstResponder];
    }
    // search for member in database
    else if (textField.tag == firstNameCellTag) {
        // check if both fields are filled
        UITextField *emailField = (UITextField *)[self.view viewWithTag:emailCellTag];
        UITextField *nameField = (UITextField *)[self.view viewWithTag:firstNameCellTag];
        
        if (! enteringPassword) {
            // check that both fields are filled
            if ([nameField.text isEqualToString:@""] ||
                [emailField.text isEqualToString:@""]) {
                
                // need error handling here...
                // both fields must be filled
                
                return YES;
            }
            
            Search_Member_Command *searchMemberCommand = [[Search_Member_Command alloc] init];
            [searchMemberCommand setEmail:emailField.text];
            [searchMemberCommand setFirstName:nameField.text];
            [searchMemberCommand listenForMyCompletion:self
                                              selector:@selector(searchedForMember:)
                                      notificationName:kDidCompleteSearchMemberOpNotification];
            [searchMemberCommand enqueueOperation];
        }
        
        /** user is entering a password **/
        /**/
        else {
            // check that the passwords are identical
            //
            /* passwords do not match */
            if (! [emailField.text isEqualToString:nameField.text]) {
                // passwords not the same; notifiy and have user retry
                [[[UIAlertView alloc] initWithTitle:@"Passwords Do Not Match"
                                            message:@"Retry"
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
                return YES;
            }
            
            // enter password into database
            //
            
            Save_Password_Command *savePasswordCmd = [[Save_Password_Command alloc] init];
            [savePasswordCmd setKhtcID:khtcID];
            [savePasswordCmd setPassword:emailField.text];  // emailField doubles as a password field
            [savePasswordCmd listenForMyCompletion:self
                                          selector:@selector(savePasswordCMDCompleted:)
                                  notificationName:kDidCompleteSavePasswordOpNotification];
            [savePasswordCmd enqueueOperation];
        }
    }
    
    return YES;
}

#pragma mark - UIAlertViewDelegate Protocol Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:kMemberNotFoundAlertTitle]) {
        // if user hits "Try Again" button, clear text fields and make first field
        // first responder
        
        // "Try Again" button
        if (buttonIndex == 0) {
            // don't do anything for the moment
        }
        // "New Account" button
        else if (buttonIndex == 1) {
            // push create new account view
            [self performSegueWithIdentifier:kPushCreateAccountVCSegueID sender:self];
            
        }
        
    }
    // after successfully locating user in database, request user input
    // password and confirmation.
    // change the textfields to reflect this new input
    else if ([alertView.title isEqualToString:kSuccessAlertTitle]) {
        [_topImageView setImage:[UIImage imageNamed:@"lock-icon_03.png"]];
        [_bottomImageView setImage:[UIImage imageNamed:@"lock-icon_03.png"]];
        [_userPrompt setText:@"now make a password for your account"];
        
        // clear fields and change placeholders
        UITextField *topField = (UITextField *)[self.view viewWithTag:emailCellTag];
        UITextField *bottomField = (UITextField *)[self.view viewWithTag:firstNameCellTag];
        [topField setText:@""];
        [topField setPlaceholder:@"password"];
        [topField setSecureTextEntry:YES];
        [bottomField setText: @""];
        [bottomField setPlaceholder:@"confirm password"];
        [bottomField setSecureTextEntry:YES];
        
        enteringPassword = YES;
    }
    else if ([alertView.title isEqualToString:kDifferentPasswordsAlertTitle]) {
        UITextField *topField = (UITextField *)[self.view viewWithTag:emailCellTag];
        UITextField *bottomField = (UITextField *)[self.view viewWithTag:firstNameCellTag];
        [topField setText:@""];
        [bottomField setText: @""];
    }
    else if ([alertView.title isEqualToString:kAccountCreatedAlertTitle]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        
        // Need to save/create new user profile here and
        // push Runs_View_Controller
    }
}

#pragma mark - Notification Methods

// Notification: kDidCompleteSearchMemverOpNotification
// Sent by Search_Member_Command object after searching for user
// email and first name in database
- (void)searchedForMember:(NSNotification *)notification
{
    Search_Member_Command *searchMemberOp = [notification object];
    
    if (searchMemberOp.status == kSuccess) {
        // have user enter password for account
        // save password into atcj.org database
        
        // -- check the returned id value --
        // if id is zero, then NO person matches the user's entered email and first name.
        // otherwise, the id uniquely identifies the user in the server database
        NSDictionary *results = [notification userInfo];
        khtcID = [results objectForKey:@"id"];
        
        if ([khtcID isEqualToString:@"0"]) {
            // no match found
            NSLog(@"id should be zero: id = %@", khtcID);
            // alert view
            // retry or use new email
            NSString *alertMessage = @"Try a new search, or create a new account with any email you like.";
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"Email/Name Not Found"
                                            message:alertMessage
                                           delegate:self
                                  cancelButtonTitle:@"Try Again"
                                  otherButtonTitles:@"New Account", nil] show];
            });
        }
        // user found in database
        else {
            NSLog(@"user's khtc id: %@", khtcID);
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"Success"
                                            message:@"We found you!"
                                           delegate:self
                                  cancelButtonTitle:@"ok"
                                  otherButtonTitles:nil] show];
            });
            
            // change text fields to password input fields
            // have user input password
        }
    }
    // operation failure
    else {
        // Error handling here.
        // Call to script on server failed; not able to check for user in database.
        //
    }
}

// Notification: kDidCompleteSavePasswordOpNotification
// Sent by Save_Password_Command object after completion
- (void)savePasswordCMDCompleted:(NSNotification *)notification
{
    Save_Password_Command *savePasswordCmd = [notification object];
    
    if (savePasswordCmd.status == kSuccess) {
        // user has successfully created an account
        // inform and pop all views from root VC
        // use an alert view for now...
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:@"Account Created!"
                                        message:nil
                                       delegate:self
                              cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        });
        
    } else {
        // error handling here...
        NSLog(@"failed to save password into database");
        abort();
    }
}

@end
