//
//  CreateAmberAlertCommand.m
//  KHTC
//
//  Created by Hans Yelek on 7/1/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "CreateAmberAlertCommand.h"
#import "NSData+Base64URL.h"
#import "AmberAlert.h"
#import "Constants.h"

@implementation CreateAmberAlertCommand

@synthesize amberAlert = _amberAlert;

- (void)main
{
    // Create JSON Payload
    UIImage *animalImage = _amberAlert.animalImage;
    
    // Converting UIImage to NSData: http://stackoverflow.com/questions/6476929/convert-uiimage-to-nsdata
    NSData *imageData = UIImageJPEGRepresentation(animalImage, 0.5);  // serialize
    NSString *imageDataString = [imageData base64URLEncoding];  // base64 encode
    // Make URL-safe: replace + with - and / with _
    imageDataString = [imageDataString stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
    imageDataString = [imageDataString stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    
    NSString *imageDataStringURLEncoded = [imageDataString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];     // url encode
    
    NSNumber *latitude = [NSNumber numberWithDouble:_amberAlert.lastKnownLocationCoordinate.latitude];
    NSNumber *longitude = [NSNumber numberWithDouble:_amberAlert.lastKnownLocationCoordinate.longitude];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:
                             _amberAlert.animalName, @"animalName",
                             _amberAlert.animalBreed, @"animalBreed",
                             @"cat", @"animalType",
                             _amberAlert.contactPhone, @"contactPhone",
                             _amberAlert.lastKnownLocationName, @"location",
                             imageDataStringURLEncoded, @"animalImage",
                             latitude, @"latitude",
                             longitude, @"longitude",
                             nil];
    
    NSError *error = nil;
    NSData *dataPayload = [NSJSONSerialization dataWithJSONObject:payload
                                                          options:NSJSONWritingPrettyPrinted
                                                            error:&error];
    if (error != nil) {
        NSLog(@"Error Creating JSON Object: %@", [error localizedDescription]);
        // error handling here...
        abort();
    }
    
    // Send new alert to php script
    NSURL *url = [NSURL URLWithString:@"http://atcj.org/KHTC/addAmberAlert.php"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:dataPayload];
    [request setHTTPMethod:@"POST"];
    
    NSHTTPURLResponse *response = nil;
    NSData *data = [self sendSynchronousRequest:request
                                     response_p:&response
                                          error:&error];
    NSLog(@"response data: %@", data);
    NSLog(@"data string: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    
    // Check for OS Error
    if (error != nil) {
        NSLog(@"OS Error: %@", [error localizedDescription]);
        NSLog(@"HTTP Status Code: %ld", (long)[response statusCode]);
        NSLog(@"HTTP Body: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        // error handling here...
        abort();
    }
    
    self.httpStatusCode = [response statusCode];
    NSDictionary *responsePayload;
    
    // Check for HTTP Error
    // OK
    if (self.httpStatusCode == 200) {
        responsePayload =
        [NSJSONSerialization JSONObjectWithData:data
         
                                        options:NSJSONReadingMutableContainers
         
                                          error:&error];
        
        // Check for server app error
        if ([responsePayload objectForKey:@"error"]) {
            NSString *errorCode = [[responsePayload objectForKey:@"error"] objectForKey:@"code"];
            NSString *errorMessage = [[responsePayload objectForKey:@"error"] objectForKey:@"message"];
            NSLog(@"Error Code: %@\nError Message: %@", errorCode, errorMessage);
            
            self.status = kFailure;
        }
        // No error: Successful Request
        else {
            self.status = kSuccess;
        }
    }
    // HTTP Error
    else {
        NSLog(@"HTTP Error: %ld", (long)[response statusCode]);
        NSLog(@"Response Payload: %@, %@",response, [response description]);
        
        self.status = kFailure;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompleteCreateAmberAlertOpNotification
                                                        object:self
                                                      userInfo:responsePayload];
}

@end
