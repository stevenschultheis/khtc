//
//  Save_Password_Command.m
//  KHTC
//
//  Created by Hans Yelek on 8/28/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "Save_Password_Command.h"
#import "Constants.h"

#define kTargetURLPath @"http://atcj.org/KHTC/Account/save_member_password.php"

@implementation Save_Password_Command

@synthesize khtcID = _khtcID;
@synthesize password = _password;

- (void)main
{
    // Create JSON payload
    //
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:
                             _khtcID, @"id",
                             _password, @"password", nil];
    NSError *error = nil;
    NSData *dataPayload = [NSJSONSerialization dataWithJSONObject:payload
                                                          options:NSJSONWritingPrettyPrinted
                                                            error:&error];
    if (error != nil) {
        NSLog(@"Error Creating JSON Object: %@", [error localizedDescription]);
        // error handling here...
        return;
    }
    
    NSURL *url = [NSURL URLWithString:kTargetURLPath];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPBody:dataPayload];
    [request setHTTPMethod:@"POST"];
    
    NSHTTPURLResponse *response = nil;
    NSData *data = [self sendSynchronousRequest:request
                                     response_p:&response
                                          error:&error];
    NSLog(@"RESPONSE DATA: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    
    // Check for OS error
    if (error != nil) {
        NSLog(@"OS Error: %@", [error localizedDescription]);
        NSLog(@"HTTP Status Code: %ld", (long)[response statusCode]);
        NSLog(@"HTTP Body: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        // error handling here...
        return;
    }
    
    self.httpStatusCode = [response statusCode];
    NSDictionary *responsePayload = nil;
    
    // Check for HTTP error
    /* OK */
    if (self.httpStatusCode == 200) {
        
        responsePayload = [NSJSONSerialization JSONObjectWithData:data
                                                          options:NSJSONReadingMutableContainers
                                                            error:&error];
        // Check for error in server script
        if ([responsePayload objectForKey:@"error"]) {
            // nslogs for debug...
            self.status = kFailure;
        }
        self.status = kSuccess;
    }
    /* HTTP Error */   
    else {
        NSLog(@"HTTP Error: %ld", (long)[response statusCode]);
        
        self.status = kFailure;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidCompleteSavePasswordOpNotification
                                                        object:self
                                                      userInfo:responsePayload];
}

@end
