//
//  FacebookUserProfile.h
//  KHTC
//
//  Created by Hans Yelek on 6/16/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    User's Facebook Profile
 
     - can be saved in UserProfile.xcdatamodeld
     - users' profiles are NOT currently saved!
 
 *******/

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FacebookUserProfile : NSManagedObject

@property (nonatomic, retain) NSString * carMake;
@property (nonatomic, retain) NSString * carModel;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSNumber * isSignedIn;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * facebookID;
@property (nonatomic, retain) NSData   * profileImageData;

@end
