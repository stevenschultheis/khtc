//
//  AlertViewController.m
//  KHTC
//
//  Created by Hans Yelek on 8/23/13.
//  Copyright (c) 2013 company. All rights reserved.
//
#import <MapKit/MapKit.h>
#import "AlertViewController.h"
#import "AmberAlert.h"

/*
    TO DO LIST:
        - set a date posted property for AmberAlert class
 */

@interface AlertViewController ()

@end

@implementation AlertViewController

@synthesize amberAlert = _amberAlert;
@synthesize animalImage= _animalImage;
@synthesize nameLabel = _nameLabel;
@synthesize imageView = _imageView;
@synthesize datePostedLabel = _datePostedLabel;
@synthesize locationLabel = _locationLabel;
@synthesize mapView_ = _mapView_;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setAlertData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AlertViewController Methods

- (void)setAlertData
{
    [_nameLabel setText:[_amberAlert objectForKey:@"name"]];
    [_datePostedLabel setText:@"09/03/13"];
    [_imageView setImage:_animalImage];
    [_locationLabel setText:[_amberAlert objectForKey:@"location"]];
    [_phoneLabel setText:[_amberAlert objectForKey:@"contactPhone"]];
    
    [self setMap];
}

- (void)setMap
{
    MKCoordinateSpan span;
    span.latitudeDelta = 0.03;
    span.longitudeDelta = 0.03;
    
    CLLocationCoordinate2D center;
    center.latitude = [[_amberAlert objectForKey:@"latitude"] doubleValue];
    center.longitude = [[_amberAlert objectForKey:@"longitude"] doubleValue];

    
    MKCoordinateRegion coordinateRegion;
    coordinateRegion.center = center;
    coordinateRegion.span = span;
    
    // center point on animal's last known location
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = center;
    
    [_mapView_ setRegion:coordinateRegion];
    [_mapView_ addAnnotation:point];
    [_mapView_ setZoomEnabled:NO];
    [_mapView_ setScrollEnabled:NO];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end