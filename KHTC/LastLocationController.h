//
//  LastLocationController.h
//  KHTC
//
//  Created by Hans Yelek on 7/3/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    LastLocationController
 
    * This view controller is used when the user wished to select a last known location
    * for a missing animal. 
    * APIs used with this controller:
        - Google Places Autocomplete API
        - Google Places Text Search API
 
    IMPORTANT NOTE:
    The query limits for these APIs are low as the app is currently treated as "in development".
    In order to increase the limits, a credit card number will have to be provided to Google, though no payment
    is required.
 
    The current Google API key is under Hans' Google account. (The key is in GoogleAPIConstants.h) This will have 
    to change before the app is made public.
 
 *******/

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@protocol LastLocationControllerDelegate <NSObject>
@required
- (void)lastLocationControllerDidReturnWithPlace:(NSString *)place andCoordinates:(CLLocationCoordinate2D)coordinates;
@end

@interface LastLocationController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *places;     // holds description of places returned from Google Places Autocomplete API
    BOOL useLocationViewVisible;
    BOOL cancelButtonPressed;
    
    NSString *placeName;
    CLLocationCoordinate2D location;
}

//@property (weak, nonatomic) IBOutlet UITableView *locationsTableView;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView_;
@property (weak, nonatomic) IBOutlet UITableView *locationsTableView;
@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@property (weak, nonatomic) IBOutlet UIView *cancelButtonView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet UIView *useLocationAlertView;

@property (weak, nonatomic) id<LastLocationControllerDelegate>delegate;


- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)useLocation:(id)sender;
- (IBAction)rejectLocation:(id)sender;


@end
