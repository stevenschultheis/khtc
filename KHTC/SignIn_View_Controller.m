//
//  SignIn_View_Controller.m
//  KHTC
//
//  Created by Hans Yelek on 8/23/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "SignIn_View_Controller.h"
#import "LoginCommand.h"
#import "Constants.h"

@interface SignIn_View_Controller ()

@end

typedef enum {
    emailTextFieldTag = 0,
    passwordTextFieldTag
} textFieldTags;

@implementation SignIn_View_Controller

@synthesize emailTextField = _emailTextField;
@synthesize passwordTextField = _passwordTextField;
@synthesize signInBtn = _signInBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate Protocol Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField.tag == emailTextFieldTag) {
        UITextField *passwordField = (UITextField *)[self.view viewWithTag:passwordTextFieldTag];
        [passwordField becomeFirstResponder];
    } else if (textField.tag == passwordTextFieldTag) {
/*
    If _passwordTextField is the current text field, should call signIn:(id)sender
    after user presses return. That is, the user should automatically log in
    after hitting return.
 */
    }
    return YES;
}

#pragma mark - Notification Methods

- (void)loginCommandCompleted:(NSNotification *)notification
{
    
    LoginCommand *loginCmd = [notification object];
    
    if (loginCmd.status == kSuccess) {
        // send user directly to run VC
        NSLog(@"PUSH RUN VC");
        dispatch_async(dispatch_get_main_queue(), ^{
            //[_signInBtn setEnabled:YES];
            
            /*
                Login is successful here. Push the Runs_View_Controller to display
                user's run(s).
             */
        });
    }
    // login failed: invalid username/password
    else {
        // notify user of invalid username/password
        dispatch_async(dispatch_get_main_queue(), ^{
            [_signInBtn setEnabled:YES];
            
            [[[UIAlertView alloc] initWithTitle:@"Invalid Email/Password"
                                        message:nil
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] show];
        });
    }
}

#pragma mark - IBAction Methods

- (IBAction)signIn:(id)sender
{
    [_signInBtn setEnabled:NO];
    
    // form validation here
    // ...
    
    
    LoginCommand *loginCmd = [[LoginCommand alloc] init];
    [loginCmd setEmail:_emailTextField.text];
    [loginCmd setPassword:_passwordTextField.text];
    [loginCmd listenForMyCompletion:self
                           selector:@selector(loginCommandCompleted:)
                   notificationName:kDidCompleteSignInOpNotification];
    [loginCmd enqueueOperation];
}
@end
