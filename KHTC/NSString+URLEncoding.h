//
//  NSString+URLEncoding.h
//  KHTC
//
//  Created by Hans Yelek on 7/1/13.
//  Copyright (c) 2013 company. All rights reserved.
//

// *****************************************************************
//
// This NSString category is borrowed from an example given at
// http://stackoverflow.com/questions/8088473/url-encode-an-nsstring
// provided by stackoverflow user 'chown'
// * Also credited to stackoverflow user 'Dave DeLong'
// *****************************************************************

#import <Foundation/Foundation.h>

@interface NSString (URLEncoding)

@end
