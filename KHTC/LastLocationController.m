//
//  LastLocationController.m
//  KHTC
//
//  Created by Hans Yelek on 7/3/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "LastLocationController.h"
#import "GooglePlacesAutoCompleteCommand.h"
#import "GooglePlacesAPICommand.h"
#import "GoogleAPIConstants.h"
#import "Constants.h"
#import "AmberAlert.h"


@interface LastLocationController ()

@end

@implementation LastLocationController

@synthesize mapView_ = _mapView_;
@synthesize locationLabel = _locationLabel;
@synthesize locationsTableView = _locationsTableView;
@synthesize locationTextField = _locationTextField;
@synthesize cancelButtonView = _cancelButtonView;
@synthesize cancelButton = _cancelButton;
@synthesize useLocationAlertView = _useLocationAlertView;
@synthesize delegate = _delegate;

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setGoogleMap];
    
    cancelButtonPressed = NO;
    [_cancelButton setEnabled:NO];
    [_locationTextField setReturnKeyType:UIReturnKeySearch];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(adjustLabelForTextField:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    
    // Google Places Autocomplete API Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(googleAutoCompleteAPIRequestCompleted:)
                                                 name:kDidCompleteGooglePlacesAutoCompleteOpNotification
                                               object:nil];
    
    // Google Places Text Search API Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(googlePlacesTextSearchAPIRequestCompleted:)
                                                 name:kDidCompleteGooglePlacesTextSearchOpNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - LastLocationController Methods

- (void)setGoogleMap
{
    [_mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:-33.86 longitude:151.20 zoom:6]];

    
    // Creates a marker in the center of the map
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
    marker.title = @"Sydney";
    marker.snippet = @"Australia";
    marker.map = _mapView_;
}

- (void)animateViewsForTextFieldSearch
{
    [UIView animateWithDuration:0.3 animations:^{

        [_locationsTableView setFrame:CGRectMake(0, 50, 320, 194)];
        
        [_locationTextField setFrame:CGRectMake(20, 2, 199, 46)];
        [_locationLabel setFrame:CGRectMake(20, 2, 199, 46)];
        
        [_cancelButtonView setFrame:CGRectMake(227, 2, 73, 46)];
        [_cancelButton setFrame:CGRectMake(1, 1, 71, 44)];
        [_cancelButtonView setBackgroundColor:[UIColor darkGrayColor]];
        [_cancelButton setEnabled:YES];
    }];
}

- (void)animateViewsForMapView
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         [_locationTextField setFrame:CGRectMake(20, 2, 280, 46)];
                         [_locationLabel setFrame:CGRectMake(20, 2, 280, 46)];
                         [_locationsTableView setFrame:CGRectMake(320, 50, 320, 194)];
                         [_cancelButtonView setFrame:CGRectMake(320, 2, 73, 46)];
                         
                         [_cancelButton setEnabled:NO];
                         [_locationTextField setText:@""];
                         [_locationLabel setText:@" Search by address, city, place, etc."];
                     }];
}

- (void)setMapWithCoordinates:(CLLocationCoordinate2D)coordinates
{
    [_mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:coordinates.latitude
                                                                   longitude:coordinates.longitude
                                                                        zoom:12.0]];
}

- (void)dropMarkerOnCoordinate:(CLLocationCoordinate2D)coordinate withTitle:(NSString *)title
{
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = coordinate;
    marker.title = title;
    marker.map = _mapView_;
}

- (void)showUseLocationView
{
    useLocationViewVisible = YES;
    [UIView animateWithDuration:0.3
                          delay:2.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
        [_useLocationAlertView setFrame:CGRectMake(0, 291, 320, 169)];

    }
                     completion:nil];
}

- (void)hideUseLocationView
{
    useLocationViewVisible = NO;
    [UIView animateWithDuration:0.3 animations:^{
        [_useLocationAlertView setFrame:CGRectMake(0, 461, 320, 169)];
    }];
}

#pragma mark - UITextFieldDelegate Protocol Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // dismiss keyboard
    // send the text in the text field to the Google Places API to get
    // the latitude and longitude coordinates
    // will send the same command if someone selects a place from a cell in the table view
    
    if (textField.hasText) {
        placeName = textField.text;
        // send Google Places API request
        GooglePlacesAPICommand *googlePlacesAPICommand = [[GooglePlacesAPICommand alloc] init];
        [googlePlacesAPICommand setPlace:placeName];
        
        [googlePlacesAPICommand enqueueOperation];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateViewsForTextFieldSearch];
    
    if (useLocationViewVisible) [self hideUseLocationView];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateViewsForMapView];
    if (! cancelButtonPressed) {
        [self showUseLocationView];
    } else {
        cancelButtonPressed = NO;
    }
}

#pragma mark - UITableViewDataSource Protocol Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%@ called", NSStringFromSelector(_cmd));
    return [places count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@ called", NSStringFromSelector(_cmd));
    static NSString *cellIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [cell.textLabel setFont:[UIFont fontWithName:@"Avenir-Heavy" size:19.0]];
    [cell.textLabel setText:[places objectAtIndex:[indexPath row]]];
    
    return cell;
}

#pragma mark - UITableViewDelegate Protocol Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    placeName = [places objectAtIndex:indexPath.row];
    
    // Send request to Google Places API to get latitude and longitude coordinates
    GooglePlacesAPICommand *googlePlacesAPICommand = [[GooglePlacesAPICommand alloc] init];
    [googlePlacesAPICommand setPlace:placeName];
    [googlePlacesAPICommand enqueueOperation];
    
    [_locationTextField resignFirstResponder];
    [self animateViewsForMapView];
}

#pragma mark - Notification Methods

// Notification: UITextFieldTextDidChangeNotification
//
- (void)adjustLabelForTextField:(NSNotification *)notification
{
    UITextField *textField = [notification object];
    
    if (textField == _locationTextField) {
        if ([textField.text isEqualToString:@""]) {
            _locationLabel.text = @" Search by address, city, place, etc.";
        }
        else {
            _locationLabel.text = @"";
            placeName = _locationTextField.text;
            // Send Google Places Autocomplete API Request
            GooglePlacesAutoCompleteCommand *autoCompleteCommand = [[GooglePlacesAutoCompleteCommand alloc] init];
            [autoCompleteCommand setPlace:placeName];
            
            [autoCompleteCommand enqueueOperation];
        }
    }
}

// Notification: kDidCompleteGooglePlacesAutoCompleteOpNotification
// Notification sent by GooglePlacesAutoCompleteCommand upon completion
/*
   The purpose of the GooglePlacesAutoCompleteCommand request is to collect possible places
    the user's searching for into a table view. The user may optionally tap one of these locations
    if the desired location is displayed.
 */
- (void)googleAutoCompleteAPIRequestCompleted:(NSNotification *)notification
{
    NSLog(@"%@ called", NSStringFromSelector(_cmd));
    
    if ([[notification object] isKindOfClass:[GooglePlacesAutoCompleteCommand class]]) {
        GooglePlacesAutoCompleteCommand *command = [notification object];
        
        if (command.status == kSuccess) {
            NSDictionary *responsePayload = [notification userInfo];
            NSString *googleAPIStatus = [responsePayload objectForKey:@"status"];
            
            // Check Google Places Autocomplete API Status
            if ([googleAPIStatus isEqualToString:kOK]) {
                
                // print payload to console
                NSLog(@"Google Places Autocomplete Payload:\n%@", responsePayload);
                
                // grab the top ten predictions and place them into an array
                // to display in the table view
                //if (places == nil)
                places = [NSMutableArray array];
                
                NSArray *predictions = [responsePayload objectForKey:@"predictions"];
                int i = 0;
                for (NSDictionary *prediction in predictions) {
                    
                    [places addObject:[prediction objectForKey:@"description"]];
                    i++;
                    
                    if (i >= 10) break;
                }
                
                // refresh tableview with data in places array
                //[self.locationsTableView reloadData];
                [self.locationsTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // NOTE: when using [self.locationsTableView reloadData] above, the table view would only refresh
                // after scrolling it a pixel or two. The performSelectorOnMainThread:withObject:waitUntilDone:
                // method resolves this problem.
                // See Stack Overflow:
                // link: http://stackoverflow.com/questions/4968424/tableview-reloaddata-doesnt-work-until-i-scroll-the-tableview
                
            } else if ([googleAPIStatus isEqualToString:kZERO_RESULTS]) {
                // refresh tableview with zero results
            } else if ([googleAPIStatus isEqualToString:kREQUEST_DENIED]) {
                NSLog(@"Google Places Autocomplete API REQUEST DENIED");
                // error handling here...
            } else if ([googleAPIStatus isEqualToString:kINVALID_REQUEST]) {
                NSLog(@"INVALID Google Places Autocomplete API Request");
                // error handling here...
            } else if ([googleAPIStatus isEqualToString:kOVER_QUERY_LIMIT]) {
                NSLog(@"OVER QUERY LIMIT!");
                // error handling here...
                // perhaps could call another api here
            
            }
        }
        // command failed
        else {
            
        }
    }
}

/* 
    - Called after a user chooses a location. The location string is sent to the Google Places API.
        This method is run after the reqest completes. The method's notification object holds relevant
        data about the location including lat/lng coordinates
 */
- (void)googlePlacesTextSearchAPIRequestCompleted:(NSNotification *)notification
{
    GooglePlacesAPICommand *googlePlacesAPICommand = [notification object];
    
    if (googlePlacesAPICommand.status == kSuccess) {
        NSDictionary *responsePayload = [notification userInfo];
        
        NSString *googleAPIStatus = [responsePayload objectForKey:@"status"];
        
        // Check Google Places Text Search API Status
        if ([googleAPIStatus isEqualToString:kOK]) {
            // pull latitude and longitude coordinates from the first result
            NSArray *results = [responsePayload objectForKey:@"results"];
            NSDictionary *firstResult = [results objectAtIndex:0];
            
            NSNumber *lat = (NSNumber *)[[[firstResult objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"];
            NSNumber *lng = (NSNumber *)[[[firstResult objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"];
            
            CLLocationDegrees latitude = [lat doubleValue];
            CLLocationDegrees longitude = [lng doubleValue];
            location = CLLocationCoordinate2DMake(latitude, longitude);
          //  [self showUseLocationView];
            [self setMapWithCoordinates:location];
            [self dropMarkerOnCoordinate:location withTitle:[googlePlacesAPICommand place]];
          //  [self showUseLocationView];
        } else if ([googleAPIStatus isEqualToString:kZERO_RESULTS]) {
            // refresh tableview with zero results
        } else if ([googleAPIStatus isEqualToString:kREQUEST_DENIED]) {
            NSLog(@"Google Places Autocomplete API REQUEST DENIED");
            // error handling here...
        } else if ([googleAPIStatus isEqualToString:kINVALID_REQUEST]) {
            NSLog(@"INVALID Google Places Autocomplete API Request");
            // error handling here...
        } else if ([googleAPIStatus isEqualToString:kOVER_QUERY_LIMIT]) {
            // error handling here...
        }
    }
    // command failed
    else {
        
    }
}

#pragma mark - IBAction Methods

- (IBAction)cancelButtonPressed:(id)sender
{
    cancelButtonPressed = YES;
    
    // dismiss keyboard,
    if ([_locationTextField isFirstResponder]) {
        [_locationTextField resignFirstResponder];
     //   [self animateViewsForMapView];
    } else {
        
    }
}

- (IBAction)useLocation:(id)sender
{
    //[self hideUseLocationView];
    
    AmberAlert *amberAlert = [AmberAlert sharedAmberAlert];
    
    [amberAlert setLastKnownLocationName:placeName];
    [amberAlert setLastKnownLocationCoordinate:location];
    
    [_delegate lastLocationControllerDidReturnWithPlace:placeName andCoordinates:location];
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)rejectLocation:(id)sender
{
    [self hideUseLocationView];
    placeName = nil;
    location = CLLocationCoordinate2DMake(0.0, 0.0);
}


@end