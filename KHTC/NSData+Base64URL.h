//
//  NSData+Base64URL.h
//  KHTC
//
//  Created by Hans Yelek on 7/2/13.
//  Copyright (c) 2013 company. All rights reserved.
//

// ***************************************************
//
// This category is modified from the following:
// Professional iOS Network Programming
// ISBN: 978-1-118-36240-2
// Authors: Jack Cox, Nathan Jones, John Szumski
// MobileBanking Application
//
// ***************************************************

#import <Foundation/Foundation.h>

@interface NSData (Base64URL)

+ (NSData *)dataWithBase64URLEncodedString:(NSString *)string;
- (id)initWithBase64URLEncodedString:(NSString *)string;

- (NSString *)base64URLEncoding;
- (NSString *)base64URLEncodingWithLineLength:(NSUInteger)lineLength;

@end
