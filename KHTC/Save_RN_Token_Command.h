//
//  Save_RN_Token_Command.h
//  KHTC
//
//  Created by Hans Yelek on 9/28/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    Save_RN_Token_Command
 
        - saves a remote notification token assigned to a user's device
        - the tokens are required if a user allows remote notifications
 
 *******/

#import "BaseCommand.h"

@interface Save_RN_Token_Command : BaseCommand

@property (nonatomic, strong) NSString * khtcID;
@property (nonatomic, strong) NSString *token;


@end
