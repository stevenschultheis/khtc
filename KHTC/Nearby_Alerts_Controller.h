//
//  Nearby_Alerts_Controller.h
//  KHTC
//
//  Created by Hans Yelek on 8/20/13.
//  Copyright (c) 2013 company. All rights reserved.
//


/* *^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^*^
 
    (This controller has a view on storyboard)
 
 Nearby_Alerts_Controller:
 *
 *  Displays missing animal alerts to the user. If user has a prefered search radius/location,
 *  the alerts in this area are given. Otherwise, alerts in the user's current location are given.
 *  
 *  If the user's does not allow for current location access, a default location for alerts needs
 *  be chosen. This has not yet been done.
 *
 *  NOTE: missing animal alerts throughout have been given the unfortunate name "Amber Alerts"
 *          These alerts of course refer to animals, not children.
 
 */
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

#import "AlertPreferences.h"

@class GMSMapView;

@interface Nearby_Alerts_Controller : UITableViewController <NSFetchedResultsControllerDelegate, CLLocationManagerDelegate, GMSMapViewDelegate>
{
    NSArray *amberAlerts;
    NSMutableArray *amberAlertImages;
    
    UITableView *_tableView;     // the default table view displaying alert lists
    
    GMSMapView *mapView_;       // map for viewing nearby alerts
    NSMutableArray *gmsAlertMarkers;
    
    BOOL usingSignificantChangeLocationService;
    BOOL usingStandardLocationService;
}

@property (nonatomic, strong) NSFetchedResultsController *fetchedAlertPreferencesController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) CLLocationManager *locationManager;

@property (nonatomic, strong) AlertPreferences *alertPreferences;

@end
