//
//  AlertPreferences.h
//  KHTC
//
//  Created by Hans Yelek on 8/20/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    AlertPreferences
 
        * contains the user's search radius and center location preferences when
            searches for amber alerts are made.
 
        NOTE:
            - this class is not currently enabled in the application
            - initial plans were to enable preference selection after selecting toolbar button 
                located at the bottom of the NearbyAlertsController view
 
            - if these preferences are enabled, they should be saved in to the user's device
 
 *******/

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AlertPreferences : NSManagedObject

@property (nonatomic, retain) NSNumber * locationLat;
@property (nonatomic, retain) NSNumber * locationLong;
@property (nonatomic, retain) NSString * locationName;
@property (nonatomic, retain) NSNumber * searchRadius;

@end
