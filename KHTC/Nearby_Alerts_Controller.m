//
//  Nearby_Alerts_Controller.m
//  KHTC
//
//  Created by Hans Yelek on 8/20/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*
 TO DO LIST
 
    - disable bar button items until alerts finished loading
 */

/*
 NOTES:
    ERROR occurs when quickly pushing and popping this view controller:
    - [Nearby_Alerts_Controller retain]: message sent to deallocated instance 0x________ -
 */

#import <GoogleMaps/GoogleMaps.h>

#import "Nearby_Alerts_Controller.h"
#import "AlertViewController.h"
#import "AppDelegate.h"
#import "GetAmberAlertsCommand.h"
#import "NSData+Base64URL.h"
#import "Constants.h"

#define kDefaultSearchRadiusInMiles 25.0

@interface Nearby_Alerts_Controller ()

@end

typedef enum {
    imageViewTag = 1,
    nameTag,
    lastLocationTag,
    datePostedTag
} CellTags;

@implementation Nearby_Alerts_Controller

@synthesize fetchedAlertPreferencesController = _fetchedAlertPreferencesController;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize locationManager = _locationManager;
@synthesize alertPreferences = _alertPreferences;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    usingSignificantChangeLocationService = NO;
    usingStandardLocationService = NO;

    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    _tableView = self.tableView;
    
    [mapView_ setDelegate:self];

    
    //[self setToolbar];
    [self fetchAlerts];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setToolbar];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setToolbarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Nearby_Alerts_Controller Methods

- (void)setToolbar
{
    // set toolbar
    //
    UIBarButtonItem *listItem = [[UIBarButtonItem alloc] initWithTitle:@"list"
                                                                 style:UIBarButtonItemStyleBordered
                                                                target:self
                                                                action:@selector(displayAlertList)];
    UIBarButtonItem *mapItem = [[UIBarButtonItem alloc] initWithTitle:@"map"
                                                                style:UIBarButtonItemStyleBordered
                                                               target:self
                                                               action:@selector(displayAlertMap)];
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                target:nil
                                                                                action:nil];
    [fixedSpace setWidth:20.0];
    
    UIToolbar *toolbar = self.navigationController.toolbar;
    [toolbar setBarStyle:UIBarStyleBlackTranslucent];
    
    [self setToolbarItems:@[listItem, fixedSpace, mapItem]];
    [self.navigationController setToolbarHidden:NO];
    
    // add icons/buttons over toolbar
    //
    //CGFloat windowHeight = self.view.window.frame.size.height;
    CGFloat toolbarHeight = self.navigationController.toolbar.frame.size.height;
    /* search location icon */
    
    UIImage *locationImage = [UIImage imageNamed:@"alert_location_icon_03.png"];
    UIImageView *locationIcon = [[UIImageView alloc] initWithImage:locationImage];
    [locationIcon setFrame:CGRectMake(203.0, toolbarHeight - 33.0, 22, 22)];
    [locationIcon setContentMode:UIViewContentModeScaleAspectFit];
    
    UIButton *locationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [locationBtn setFrame:CGRectMake(192.0, toolbarHeight - 44.0, 44.0, 44.0)];
    [locationBtn setShowsTouchWhenHighlighted:YES];
    
    /* radius icon */
    UIImage *radiusImage = [UIImage imageNamed:@"alert_radius_icon_06"];
    UIImageView *radiusIcon = [[UIImageView alloc] initWithImage:radiusImage];
    [radiusIcon setFrame:CGRectMake(255.0, toolbarHeight - 33.0, 22.0, 22.0)];
    [radiusIcon setContentMode:UIViewContentModeScaleAspectFit];
    
    UIButton *radiusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [radiusBtn setFrame:CGRectMake(244.0, toolbarHeight - 44.0, 44.0, 44.0)];
    [radiusBtn setShowsTouchWhenHighlighted:YES];
    
    [toolbar addSubview:locationIcon];
    [toolbar addSubview:locationBtn];
    [toolbar addSubview:radiusIcon];
    [toolbar addSubview:radiusBtn];
}

- (void)beginSignificantLocationChangeMonitoring
{
    if (self.locationManager == nil) {
        self.locationManager = [[CLLocationManager alloc] init];
    }
    
    [self.locationManager startMonitoringSignificantLocationChanges];
    [self.locationManager setDelegate:self];
}

- (void)beginLocationUpdates
{
    if (self.locationManager == nil) self.locationManager = [[CLLocationManager alloc] init];
    
    [self.locationManager setDelegate:self];
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyKilometer];
    [self.locationManager startUpdatingLocation];
}

- (void)stopLocationUpdates
{
    if (usingSignificantChangeLocationService) {
        [self.locationManager stopMonitoringSignificantLocationChanges];
        usingSignificantChangeLocationService = NO;
    } else if (usingStandardLocationService) {
        [self.locationManager stopUpdatingLocation];
        usingStandardLocationService = NO;
    }
}

- (void)fetchAlerts {
    
    NSError *error = nil;
    if (! [self.fetchedAlertPreferencesController performFetch:&error]) {
        // Add error handling here...
        NSLog(@"Unresolved fetch error: %@, %@", error, [error userInfo]);
        abort();
    }
    
    if ([[self.fetchedAlertPreferencesController fetchedObjects] count] > 0) {
        self.alertPreferences = [[self.fetchedAlertPreferencesController fetchedObjects] objectAtIndex:0];
    }
    
    // if preferences do not exist, search for alerts near user's current location;
    // otherwise, search according to the preferred location and radius
    if (! self.alertPreferences.locationLat || ! self.alertPreferences.locationLong) {
        if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
            [self beginSignificantLocationChangeMonitoring];
            usingSignificantChangeLocationService = YES;
        } else if ([CLLocationManager locationServicesEnabled]) {
            NSLog(@"location services available");
            [self beginLocationUpdates];
            usingStandardLocationService = YES;
        }
        // allow for a default location if services not available
        else {
            /* CHOOSE DEFAULT LOCATION HERE */
        }
        
    }
    // preferences exist
    else {
        GetAmberAlertsCommand *getAlertsCommand = [[GetAmberAlertsCommand alloc] init];
        [getAlertsCommand setLatitude:self.alertPreferences.locationLat];
        [getAlertsCommand setLongitude:self.alertPreferences.locationLong];
        [getAlertsCommand setRange:self.alertPreferences.searchRadius];
        [getAlertsCommand listenForMyCompletion:self
                                       selector:@selector(getAmberAlertsOperationComplete:)
                               notificationName:kDidCompleteGetAmberAlertsOpNotification];
        [getAlertsCommand enqueueOperation];
    }
}

#pragma mark - Target - Action Methods

// called by list bar button item
-(void)displayAlertList
{
    [mapView_ setHidden:YES];
    [self setView:_tableView];
    [_tableView setHidden:NO];
}

// called by map bar button item
-(void)displayAlertMap
{
    BOOL updateMap = NO;    // is YES if map is loading for the first time
    
    if (! mapView_) {
        // currently random latitude, longitude settings
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:40.0 longitude:105.0 zoom:10.0];
        mapView_ = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
        
        updateMap = YES;
    }
    
    GMSCoordinateBounds *bounds;
    // set initial coordinates for bounds
    CLLocationCoordinate2D initialBoundCoord;
    initialBoundCoord.latitude = [[[amberAlerts objectAtIndex:0] objectForKey:@"latitude"] doubleValue];
    initialBoundCoord.longitude = [[[ amberAlerts objectAtIndex:0] objectForKey:@"longitude"] doubleValue];
    //
    CLLocationCoordinate2D initialBoundCoord2;
    initialBoundCoord2.latitude = [[[amberAlerts objectAtIndex:1] objectForKey:@"latitude"] doubleValue];
    initialBoundCoord2.longitude = [[[ amberAlerts objectAtIndex:1] objectForKey:@"longitude"] doubleValue];
    
    bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:initialBoundCoord coordinate:initialBoundCoord2];
    
    // iterate through amber alerts and set markers to map
    for (NSDictionary *amberAlert in amberAlerts) {
        NSString *name = [amberAlert objectForKey:@"name"];
        NSString *location = [amberAlert objectForKey:@"location"];
        
        CLLocationDegrees latitude = [[amberAlert objectForKey:@"latitude"] doubleValue];
        CLLocationDegrees longitude = [[amberAlert objectForKey:@"longitude"] doubleValue];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        
        // add current marker coordinate to map's bounds
        bounds = [bounds includingCoordinate:coordinate];
        
        GMSMarker *marker = [GMSMarker markerWithPosition:coordinate];
        marker.title = name;
        marker.snippet = location;
        marker.map = mapView_;
    }
    
    [self.tableView setHidden:YES];
    [self setView:mapView_];
    [mapView_ setHidden:NO];
    
    if (updateMap) {
        GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding: 100.0f];
        [mapView_ animateWithCameraUpdate:update];
    }
}

#pragma mark - CLLocationManagerDelegateMethods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = [locations lastObject];
    
    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 30.0) return;
    if (newLocation.horizontalAccuracy < 0) return;
    
    // This if block keeps the command from being called multiple times.
    // the method stopLocationUpdates sets the test values to NO
    if (usingStandardLocationService || usingSignificantChangeLocationService) {
        
        GetAmberAlertsCommand *getAlertsCommand = [[GetAmberAlertsCommand alloc] init];
        
        [getAlertsCommand setLatitude:[NSNumber numberWithDouble:newLocation.coordinate.latitude]];
        [getAlertsCommand setLongitude:[NSNumber numberWithDouble:newLocation.coordinate.longitude]];
        
        // set the search radius
        NSNumber *searchRadius;
        if (self.alertPreferences && self.alertPreferences.searchRadius) {
            searchRadius = self.alertPreferences.searchRadius;
            [getAlertsCommand setRange: searchRadius];
        } else {
            [getAlertsCommand setRange:[NSNumber numberWithDouble:kDefaultSearchRadiusInMiles]];
        }
        
        [getAlertsCommand listenForMyCompletion:self
                                       selector:@selector(getAmberAlertsOperationComplete:)
                               notificationName:kDidCompleteGetAmberAlertsOpNotification];
        [getAlertsCommand enqueueOperation];
                
    }
    
    // Monitoring only needed to collect user's initial location
    [self stopLocationUpdates];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    // Use of Location Services Denied: stop them
    if ([error code] == kCLErrorDenied) {
        [self stopLocationUpdates];
        // error handling here...
    }
    // ignore kCLErrorLocationUnknown and other errors
    return;
}

#pragma mark - Notification Methods

// Notification: kDidCompleteGetAmberAlertsOpNotification
//
- (void)getAmberAlertsOperationComplete:(NSNotification *)notification
{
    GetAmberAlertsCommand *getAlertsCommand = [notification object];
    NSDictionary *response = [notification userInfo];
    
    if (getAlertsCommand.status == kSuccess) {
        amberAlerts = [response objectForKey:@"results"];
        
        // load amber alert images into array
        amberAlertImages = [NSMutableArray array];
        for (NSDictionary *amberAlert in amberAlerts) {
            NSURL *imageFilePath = [NSURL URLWithString:[amberAlert objectForKey:@"picture"]];
            NSData *imageData = [NSData dataWithContentsOfURL:imageFilePath];
            UIImage *animalImage = [[UIImage alloc] initWithData:imageData];
            // cache the image into the local array
            [amberAlertImages addObject:animalImage];
        }     
        
    } else {
        NSLog(@"Failed to get amber alerts: %@", [response objectForKey:@"error"]);
        // error handling here...
        abort();
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

#pragma mark - Core Data Methods

- (NSFetchedResultsController *)fetchedAlertPreferencesController
{
    if (_fetchedAlertPreferencesController != nil) {
        return _fetchedAlertPreferencesController;
    }
    
    // Set up fetched results controller
    //
    // Create fetch request for the AlertPreferences entity
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *alertPreferences = [NSEntityDescription entityForName:@"AlertPreferences" inManagedObjectContext:self.managedObjectContext];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"searchRadius" ascending:YES];
    
    [request setEntity:alertPreferences];
    [request setSortDescriptors:@[sortDescriptor]];
    [request setFetchBatchSize:1];
    
    // Create the fetched results controller
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    frc.delegate = self;
    self.fetchedAlertPreferencesController = frc;
    
    return self.fetchedAlertPreferencesController;
}

#pragma mark - GMSMapViewDelegate Protocol Methods

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    // Display the alert corresponding to the map marker
    //
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (amberAlerts == nil) return 0;
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (amberAlerts == nil) return 0;
    
    return [amberAlerts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"animalAlertCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    NSDictionary *amberAlert = [amberAlerts objectAtIndex:indexPath.row];
    UIImage *animalImage = [amberAlertImages objectAtIndex:indexPath.row];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:imageViewTag];
    UILabel *animalName = (UILabel *)[cell viewWithTag:nameTag];
    [animalName setTextColor:[UIColor colorWithRed:212/255.0 green:233/255.0 blue:199/255.0 alpha:1.0]];
    UILabel *location = (UILabel *)[cell viewWithTag:lastLocationTag];
    UILabel *postDate = (UILabel *)[cell viewWithTag:datePostedTag];
    
    // get current date
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    [postDate setText:[@"Posted " stringByAppendingString:[dateFormatter stringFromDate:now]]];
    [animalName setText:[amberAlert objectForKey:@"name"]];
    [location setText:[amberAlert objectForKey:@"location"]];
    [imageView setImage:animalImage];
    
    return cell;
}

#pragma mark - UITableViewDelegate Protocol Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 226.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"viewAlertSegue"]) {
        NSInteger index = [self.tableView indexPathForSelectedRow].row;
        AlertViewController *alertVC = (AlertViewController *)[segue destinationViewController];
        [alertVC setAmberAlert:(NSDictionary *)[amberAlerts objectAtIndex:index]];
        [alertVC setAnimalImage:(UIImage *)[amberAlertImages objectAtIndex:index]];
    }
}

@end