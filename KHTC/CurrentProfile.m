//
//  CurrentProfile.m
//  KHTC
//
//  Created by Hans Yelek on 6/16/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#import "CurrentProfile.h"
#import "FacebookUserProfile.h"
#import "EmailUserProfile.h"

@implementation CurrentProfile

static CurrentProfile *_sharedCurrentProfile = nil;

@synthesize facebookProfile = _facebookProfile;
@synthesize emailProfile = _emailProfile;
@synthesize loggedInWithEmail = _loggedInWithEmail;
@synthesize loggedInWithFacebook = _loggedInWithFacebook;

#pragma mark - Class Methods

+ (CurrentProfile *)sharedCurrentProfile
{
    @synchronized([CurrentProfile class]) {
        if (! _sharedCurrentProfile)
            _sharedCurrentProfile = [[self alloc] init];
        
        return _sharedCurrentProfile;
    }
    return nil;
}

#pragma mark - Accessor Methods

- (void)setFacebookProfile:(FacebookUserProfile *)facebookProfile
{
    facebookProfile ? (_loggedInWithFacebook  = YES) : (_loggedInWithFacebook = NO);
    
    _facebookProfile = facebookProfile;
}

- (void)setEmailProfile:(EmailUserProfile *)emailProfile
{
    emailProfile ? (_loggedInWithEmail = YES) : (_loggedInWithEmail = NO);
    
    _emailProfile = emailProfile;
}

#pragma mark - CurrentProfile Methods

- (NSString *)firstName
{
    if (_loggedInWithFacebook)
        return _facebookProfile.firstName;
    else if (_loggedInWithEmail)
        return _emailProfile.firstName;
    else 
        return nil;
}

- (NSString *)lastName
{
    if (_loggedInWithFacebook)
        return _facebookProfile.lastName;
    else if (_loggedInWithEmail)
        return _emailProfile.lastName;
    else
        return nil;
}

- (NSString *)phone
{
    if (_loggedInWithFacebook)
        return _facebookProfile.phone;
    else if (_loggedInWithEmail)
        return _emailProfile.phone;
    else
        return nil;
}

- (NSString *)city
{
    if (_loggedInWithFacebook)
        return _facebookProfile.city;
    else if (_loggedInWithEmail)
        return _emailProfile.city;
    else
        return nil;
}

- (NSString *)state
{
    if (_loggedInWithFacebook)
        return _facebookProfile.state;
    else if (_loggedInWithEmail)
        return _emailProfile.state;
    else
        return nil;
}

- (NSString *)email
{
    if (_loggedInWithFacebook)
        return _facebookProfile.email;
    else if (_loggedInWithEmail)
        return _emailProfile.email;
    else
        return nil;
}

- (NSString *)password
{   // NOTE: Facebook profiles currently have no password assigned
    if (_loggedInWithEmail)
        return _emailProfile.password;
    else
        return nil;
}

- (UIImage *)profileImage
{
    UIImage *profile;
    
    if (_loggedInWithFacebook) {
        profile = [UIImage imageWithData:_facebookProfile.profileImageData];
        return profile;
    } else if (_loggedInWithEmail) {
        profile = [UIImage imageWithData:_emailProfile.profileImageData];
        return profile;
    } else {
        return nil;
    }
}

@end
