//
//  CreateAmberAlertCommand.h
//  KHTC
//
//  Created by Hans Yelek on 7/1/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
        
    CreateAmberAlertCommand
 
        - sends infomation for a new amber alert to the atcj.org database
        - the command is executed after a user creates a new amber alert through
            the application
 
 *******/

#import <Foundation/Foundation.h>
#import "BaseCommand.h"

@class AmberAlert;

@interface CreateAmberAlertCommand : BaseCommand

@property (nonatomic, strong) AmberAlert *amberAlert;

@end
