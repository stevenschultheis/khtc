//
//  Search_Member_Command.h
//  KHTC
//
//  Created by Hans Yelek on 8/23/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
    
    Search_Member_Command
 
        * searches for members' information in atcj.org database
 
         This command is made to check if a user's information is already
         stored in the database. Specifically, the command request the user's
         first name and email from a single row in the database.
 
 *******/

#import "BaseCommand.h"

@interface Search_Member_Command : BaseCommand

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *firstName;

@end
