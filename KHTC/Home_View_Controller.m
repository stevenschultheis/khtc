//
//  Home_View_Controller.m
//  KHTC
//
//  Created by Hans Yelek on 8/23/13.
//  Copyright (c) 2013 company. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "Home_View_Controller.h"
#import "Runs_View_Controller.h"
#import "Load_Runs_Command.h"
#import "Constants.h"
#import "AppDelegate.h"


#define kPushSignInVC_SegueID @"pushSignInVC"
#define kPushNoRunsVC @"pushNoRunsVC"
#define kPushRunsVC @"pushRunsVC"

@interface Home_View_Controller ()

@end

@implementation Home_View_Controller

@synthesize runs = _runs;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Home_View_Controller Methods

- (IBAction)viewTransportData:(id)sender
{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    
    // sign-in required to view transport data
    if (/*0*/(! delegate.isSignedInWithEmail) && (! delegate.isSignedInWithFacebook)) {
        [self performSegueWithIdentifier:kPushSignInVC_SegueID sender:self];
    }
    // user is signed in; check if user is assigned any runs
    else {
        Load_Runs_Command *loadRunsCmd = [[Load_Runs_Command alloc] init];
        
        // set khtcID property of loadRunsCmd here
        // ..
        [loadRunsCmd setKhtcID:[NSNumber numberWithInt:637]];
        //
        [loadRunsCmd listenForMyCompletion:self
    selector:@selector(loadRunsOpCompleted:)
                          notificationName:kDidCompleteFindRunsOpNotification];
        [loadRunsCmd enqueueOperation];
    }
}

#pragma mark - Storyboard Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kPushRunsVC]) {
        Runs_View_Controller *runsVC = (Runs_View_Controller *)[segue destinationViewController];
        // set runs dictionary
        [runsVC setRuns:_runs];
    }
}

#pragma mark - Notification Methods

/*
    If user is assigned runs, this method pushes a new view controller with
        a) the assigned single run, or
        b) a list (preferably in a table view) of all runs if assigned more than one
 */
- (void)loadRunsOpCompleted:(NSNotification *)notification
{
    Load_Runs_Command *loadRunsCmd = [notification object];
    
    /* HTTP request successful */
    if (loadRunsCmd.status == kSuccess) {
        NSDictionary *results = [notification userInfo];

        // driver is assigned to runs
        if ([results objectForKey:@"results"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _runs = results;
                [self performSegueWithIdentifier:kPushRunsVC sender:self];
            });
        }
        // driver is not assigned to runs
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self performSegueWithIdentifier:kPushNoRunsVC sender:nil];
            });
        }
    }
}
@end