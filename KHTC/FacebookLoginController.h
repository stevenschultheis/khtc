//
//  FacebookLoginController.h
//  KHTC
//
//  Created by Hans Yelek on 6/16/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*
 *  This class is not directly implemented but is inherited by view controllers
 *  using the FBLoginView to log in to Facebook. This class implements the appropriate
 *  FBLoginViewDelegate protocol methods
 */

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreData/CoreData.h>

@interface FacebookLoginController : NSObject <FBLoginViewDelegate, NSFetchedResultsControllerDelegate>
{
}

@property (nonatomic, assign) BOOL facebookLogin;

@property (nonatomic, strong) NSFetchedResultsController *fetchedFBProfilesController;
@property (nonatomic, strong) NSFetchedResultsController *fetchedEmailProfilesController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) id<FBGraphUser>facebookUser;

@end
