//
//  Email_or_FB_View_Controller.h
//  KHTC
//
//  Created by Hans Yelek on 8/30/13.
//  Copyright (c) 2013 company. All rights reserved.
//

/*******
 
    Email_or_FB_View_Controller
 
        (this controller is on storyboard)
 
        * This controller is gives the user the option to sign in with email
            or Facebook
        * selection of the facebook button automatically logs the user in to Facebook
    
 *******/

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "FacebookLoginController.h"

@interface Email_or_FB_View_Controller : UIViewController
{
    FacebookLoginController *_fbLoginController;
}

@end
