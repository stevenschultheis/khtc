//
//  Constants.h
//  KHTC
//
//  Created by Hans Yelek on 5/30/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#ifndef KHTC_Constants_h
#define KHTC_Constants_h

// Sign-In / Account Notifications
#define kDidCompleteSavePasswordOpNotification  @"SavePasswordOperationComplete"
#define kDidCompleteOperationNotification       @"SuccessfulOperationCompletion"
#define kDidCompleteSearchMemberOpNotification  @"SearchMemberOperationComplete"
#define kDidCompleteSignInOpNotification        @"SignInOperationComplete"
#define kDidCompleteCreateAccountOpNotification @"CreateAccountOperationComplete"
#define kDidCreateAccountNotification           @"AccountCreationSuccess"
#define kUserDidSignOutNotification             @"UserSignedOut"

// Amber Alert Notifications
#define kDidCompleteCreateAmberAlertOpNotification  @"CreateAmberAlertOperationComplete"
#define kDidCompleteGetAmberAlertsOpNotification    @"GetAmberAlertsOperationComplete"

// Google API Notifications
#define kDidCompleteGooglePlacesAutoCompleteOpNotification  @"GooglePlacesAutoCompleteOpCompleted"
#define kDidCompleteGooglePlacesTextSearchOpNotification    @"GooglePlacesTextSearchAPIOpCompleted"

// Facebook notifications
#define kDidReceiveFBUserInfoNotification               @"FacebookUserInfoReceived"
#define kDidStartFBAccountCreationNotification          @"FacebookAccountCreationStarted"
#define kFBUserDidLogInWithExistingAccountNotification  @"FBUserLoggedInWithExistingAccount"

// User Runs Notifications
#define kDidCompleteFindRunsOpNotification  @"LoadUserRunsOpComplete"

// Remote Notifications
#define kDidCompleteRegisterRNTokenOpNotification   @"RegisterRNTokenOpComplete"
#define kDidCompletePushNotificationOp              @"PushNotificationOpComplete"

// Segues
#define kSegueToNewAccountViewController        @"ProfileToNewAccountVC"
#define kPushToNewAccountViewControllerSegue    @"PushNewAccountVCSegue"


#endif
