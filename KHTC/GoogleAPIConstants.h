//
//  GoogleAPIConstants.h
//  KHTC
//
//  Created by Hans Yelek on 7/3/13.
//  Copyright (c) 2013 company. All rights reserved.
//

#ifndef KHTC_GoogleAPIConstants_h
#define KHTC_GoogleAPIConstants_h

// API Keys
// NOTE FROM STACK OVERFLOW: "The Google Places API does not currently support Android or iOS keys generated from the Google APIs Console. Only Server and Browser keys are currently supported."
// - Link: http://stackoverflow.com/questions/14654758/google-places-api-request-denied-for-android-autocomplete-even-with-the-right-a
#define kGoogleMapsAndPlacesAPIKey @"AIzaSyAz3DRwQpdNb4tXw7iR5mTAPrV-JLkFXtE"

// API Paths
#define kGooglePlacesAutoCompleteAPIPath    @"https://maps.googleapis.com/maps/api/place/autocomplete/json?"
#define kGooglePlacesTextSearchAPIPath      @"https://maps.googleapis.com/maps/api/place/textsearch/json?"

// Status Codes
#define kOK                 @"OK"
#define kZERO_RESULTS       @"ZERO_RESULTS"
#define kOVER_QUERY_LIMIT   @"OVER_QUERY_LIMIT"
#define kREQUEST_DENIED     @"REQUEST_DENIED"
#define kINVALID_REQUEST    @"INVALID_REQUEST"

#endif
